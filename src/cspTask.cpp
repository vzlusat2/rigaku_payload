/**
 * Mission Control Server
 *
 * @author vzlu
 */

#include <stdio.h>

#include <csp/csp.h>
#include <csp/csp_endian.h>
#include <csp/csp_cmp.h>
#include <csp/arch/csp_time.h>

#include "xray.h"
#include "Log.h"
#include "version.h"
#include "cspTask.h"
#include "WorkerThread.h"
#include "mainTask.h"
#include "detectorTask.h"
#include "ioTask.h"
#include "dkHandler.h"

#include <iostream>
#include <fstream>

using namespace std;

/*
 * Find CSP documentation here: https://github.com/libcsp/libcsp/tree/master/doc
 */

int xcsp_buf_sendto(uint8_t    prio,
		    uint8_t    dest,
		    uint8_t    dport,
		    uint8_t    src_port,
		    uint32_t   opts,
		    void     * outbuf,
		    int        outlen,
		    uint32_t   timeout)
{
  csp_packet_t *packet = (csp_packet_t *)csp_buffer_get(outlen);
  if (!packet)
    return 1; // err                                                                                                                                                                                                                          

  // copy data                                                                                                                                                                                                                                
  packet->length = outlen;
  memcpy(packet->data, outbuf, outlen);

  if(!csp_sendto(prio, dest, dport, src_port, opts, packet, timeout)) {
    return 0;
  } else {
    csp_buffer_free(packet);
    return 1; // err                                                                                                                                                                                                                          
  }
}

static void server_process_msg_get_raw(csp_packet_t *packet, csp_conn_t * conn)
{
	// might be e.g. some struct containing reply data
	char replyBuffer[1];

	fprintf(stdout, "CSP packet: src=%u, dst=%u, Dport=%u, Sport=%u\n", packet->id.src, packet->id.dst, packet->id.dport, packet->id.sport);
	
	csp_buffer_free(packet);
	packet = (csp_packet_t *)csp_buffer_get(sizeof(replyBuffer));
	if (!packet)
		return;
	packet->length = sizeof(replyBuffer);
	memcpy(packet->data, &replyBuffer, sizeof(replyBuffer));
	if (!csp_send(conn, packet, 0))
		csp_buffer_free(packet);
	csp_close(conn);
}

uint32_t get_app_version()
{
  return APP_VERSION;
}

unsigned long get_start_nb()
{
  static bool initialized = false;
  unsigned long start_nb = 0;

  if(initialized)
    return start_nb;

  start_nb = get_start_nb_dk();

  // update start number
  start_nb++;

  // save start number
  save_start_nb_dk((uint16_t)start_nb);                                                                                                                                                                                                     

  initialized = true;
  return start_nb;
}

unsigned long get_start_nbX()
{
  static bool initialized = false;
  unsigned long start_nb = 0;
  
  if(initialized)
    return start_nb;
  
  bool create_file = false;
  // read start number
  try {
    ifstream fs("/opt/rite/var/start_nb");
    if(!fs.is_open())
      throw exception();
    fs >> start_nb;
    if(fs.fail())
      throw exception();
    fs.close();
  } catch (exception& e) {
    write_log(LOG_WARNING, "Error reading start_nb");
    start_nb = 0;
    create_file = true;
  }

  if(create_file) {
    if(system("mkdir -p /opt/rite/var"))
      write_log(LOG_WARNING, "Error creating directory /opt/rite/var");
  }
   
  // update start number
  start_nb++;
  
  // save start number
  try {
    ofstream fs("/opt/rite/var/start_nb");
    if(!fs.is_open())
      throw exception();
    fs << start_nb;
    if(fs.fail())
      throw exception();
    fs.close();
  } catch (exception& e) {
    write_log(LOG_WARNING, "Error writing start_nb");
  }

  //save_start_nb_dk((uint16_t)start_nb);
  
  initialized = true;
  return start_nb;
}

global_settings_t get_global_app_settings()
{
  static global_settings_t settings;
  static bool initialized = false;

  if(!initialized) {
    settings.start_nb = get_start_nb();
    settings.version = get_app_version();
    initialized = true;
  }

  return settings;
}

// experiment setting used in mainTask
extern experiment_settings_t g_ex_settings;

// Worker thread instances
WorkerThread sCSPEventQueue("mainTask");
ConsumableValue<int32_t> sCSPAckConsumable;
WorkerThread sDetectorEventQueue("detectorTask");
WorkerThread sIOEventQueue("ioTask");
ConsumableValue<uint32_t> sCSPDKInfoAckConsumable;

extern "C" void * cspTask(void * parameters)
{
  static bool running = false;

  if (running) {
    csp_log_info("server task already started!");
    return NULL;
  }

  running = true;

  /* Allocate space for callibration matrices */
  new_global_detector_calibration();

  /* Initialize application settings */
  g_ex_settings.application = get_global_app_settings();
  
  /* Create socket */
  csp_socket_t * sock = csp_socket(CSP_SO_NONE);

  /* Bind every port to socket */
  csp_bind(sock, CSP_ANY);

  /* Create 10 connections backlog queue */
  csp_listen(sock, 10);

  /* Pointer to current connection and packet */
  csp_conn_t * conn;
  csp_packet_t * packet;

  /* Start working tasks: main, detector, io */
  sCSPEventQueue.CreateThread();
  sCSPEventQueue.SetPostHandler(mainTask);
  
  sDetectorEventQueue.CreateThread();
  sDetectorEventQueue.SetPostHandler(detectorTask);
  
  sIOEventQueue.CreateThread();
  sIOEventQueue.SetPostHandler(ioTask);

  /* Initialize experiment configuration */
  load_experiment_config_dk(true);
  
  /* Process incoming connections */
  while (1) {

    /* Wait for connection, 10000 ms timeout */
    if ((conn = csp_accept(sock, 10000)) == NULL)
      continue;

    /* Read packets. Timout is 1000 ms */
    while ((packet = csp_read(conn, 1000)) != NULL) {

      write_log(LOG_PACKET, "(CSP) src=%u, dst=%u, Dport=%u, Sport=%u", packet->id.src, packet->id.dst, packet->id.dport, packet->id.sport);
		  
      switch(csp_conn_dport(conn)) {

        case XRAY_PORT_DIRECT: {
	  sCSPEvent_t * newEvent = new sCSPEvent_t();
	  newEvent->eEventType = directEvent;
	  newEvent->pvData = packet;
	  newEvent->optConn = conn;
	  sCSPEventQueue.PostMsg(static_cast<const void*>(newEvent));
	  break;
	}
        case XRAY_PORT_DK_COM_ACK: {
	  int32_t err_no = ((dk_reply_common_t *) packet->data)->reply;
	  csp_buffer_free(packet);
	  csp_close(conn);
	  sCSPAckConsumable.produce(err_no, 10ms);
	  break;
	}
	case XRAY_PORT_DK_INFO_ACK: {
          uint32_t chunks = ((dk_reply_info_t *) packet->data)->newest_chunk_id;
	  csp_buffer_free(packet);
          csp_close(conn);
          sCSPDKInfoAckConsumable.produce(chunks, 10ms);
	  break;
        }
        default: {
	  csp_service_handler(conn, packet);
	  csp_close(conn);
	  break;
	}
      }
    }

    /* Close current connection, and handle next */
    csp_close(conn);
  }

  csp_log_info("server task done");

  /* Deallocate space for callibration matrices */
  delete_global_detector_calibration();

  return NULL;

}

int synchronize_time(int node, bool csp, bool system)
{
  extern csp_timestamp_t ts;
  extern uint32_t seconds_last;
  extern uint32_t millisec_last;
 
  struct csp_cmp_message msg = {};
  // get clock from NODE                                                                                                                                                                                                                      
  // 0 - means get time                                                                                                                                                                                                                       
  msg.clock.tv_sec = 0;
  msg.clock.tv_nsec = 0;

  if (csp_cmp_clock(node, 1000, &msg) != CSP_ERR_NONE) {
    write_log(LOG_ERROR, "Error CMP clock");
    return 1;
  }

  if(system) {
    struct timespec ts;
    ts.tv_sec = csp_ntoh32(msg.clock.tv_sec);
    ts.tv_nsec = csp_ntoh32(msg.clock.tv_nsec);
    int rv = clock_settime(CLOCK_REALTIME, &ts);
    if(rv)
      write_log(LOG_ERROR, "Error setting system time");
    else
      write_log(LOG_INFO, "Setting SYSTEM time");
  }

  if(csp) {
    seconds_last = csp_get_s();
    millisec_last = csp_get_ms();
    ts.tv_sec = csp_ntoh32(msg.clock.tv_sec);
    ts.tv_nsec = csp_ntoh32(msg.clock.tv_nsec);
    write_log(LOG_INFO,"Setting CSP time");
  }
 
  return 0;
}
