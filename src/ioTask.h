/*
 * ioTask.h
 *
 *  Author: Zdenek Matej
 */ 

#include "processing.h"

#ifndef _IOTASK_H_
#define _IOTASK_H_

typedef enum {
 XRAY_IO_TASK_VOID = TASK_VOID,
 XRAY_IO_SEND_METAINFO,
 XRAY_IO_SEND_FRAME_EVENTS,
 XRAY_IO_SEND_ENERGY_HISTOGRAM,
 XRAY_IO_SEND_HISTOGRAMS,
 XRAY_IO_SEND_BINNED_DATA,
 XRAY_IO_START_SESSION,
 XRAY_IO_END_SESSION,
 XRAY_IO_SYSTEM_SHUTDOWN,
 XRAY_IO_SEND_CONFIG,
 XRAY_IO_SEND_STATUS,
} ioEventType_t;

typedef struct IO_TASK_EVT_DTA
{
  ioEventType_t eventType; /* Tells the receiving task what the event is. */
  void *pvData; /* Holds or points to any data associated with the event. */
} ioEvent_t;

struct io_frame_data_t
{
  FRAME_DATA_TYPE * data;
  uint16_t id;
  int size;
  int height;
  int width;
  uint8_t type;
};

struct io_histogram_data_t
{
  uint8_t * data;
  uint16_t id;
  uint8_t roi_id;
  uint16_t len;
  uint8_t type;
};

struct io_energy_histogram_data_t
{
  uint16_t * data;
  uint16_t id;
  uint8_t roi_id;
  uint16_t en_max;
};

struct io_binned_data_t
{
  FRAME_DATA_TYPE * data;
  uint16_t id;
  uint32_t len;
  uint16_t type;
};

int ioTask(const void *, void *);
uint32_t getNextChunkId(uint8_t port);
uint32_t waitForDkInfoAck();

#endif /* _IOTASK_H_ */
