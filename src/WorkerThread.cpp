#include "WorkerThread.h"
#include "Fault.h"
#include <iostream>
#include <cstring>

using namespace std;

#define MSG_EXIT_THREAD			1
#define MSG_POST_USER_DATA		2
#define MSG_TIMER				3

struct ThreadMsg
{
	ThreadMsg(int i, const void* m, unsigned long osz=0):finished(false),omsg_(NULL) { id = i; msg = m; needrv = (osz>0); osize = osz; }
	int id;
	const void* msg;
	bool needrv;
	bool finished;
	int rv;
	void* omsg_;
	unsigned long osize; 
	mutex mx;
	condition_variable cv;
};

//----------------------------------------------------------------------------
// WorkerThread
//----------------------------------------------------------------------------
WorkerThread::WorkerThread(const char* threadName) : m_thread(0), m_timerExit(false), THREAD_NAME(threadName), m_postHandler(NULL), m_timerHandler(NULL)
{
}

//----------------------------------------------------------------------------
// ~WorkerThread
//----------------------------------------------------------------------------
WorkerThread::~WorkerThread()
{
	ExitThread();
}

//----------------------------------------------------------------------------
// CreateThread
//----------------------------------------------------------------------------
bool WorkerThread::CreateThread()
{
	if (!m_thread)
		m_thread = new thread(&WorkerThread::Process, this);
	return true;
}

//----------------------------------------------------------------------------
// SetPostHandler
//----------------------------------------------------------------------------
void WorkerThread::SetPostHandler(int (*func)(const void*, void*))
{
	m_postHandler = func;
}
	
//----------------------------------------------------------------------------
// SetTimerHandler
//----------------------------------------------------------------------------
void WorkerThread::SetTimerHandler(void (*func)(const void*))
{
	m_timerHandler = func;
}

//----------------------------------------------------------------------------
// GetThreadId
//----------------------------------------------------------------------------
std::thread::id WorkerThread::GetThreadId()
{
	ASSERT_TRUE(m_thread != 0);
	return m_thread->get_id();
}

//----------------------------------------------------------------------------
// GetCurrentThreadId
//----------------------------------------------------------------------------
std::thread::id WorkerThread::GetCurrentThreadId()
{
	return this_thread::get_id();
}

//----------------------------------------------------------------------------
// ExitThread
//----------------------------------------------------------------------------
void WorkerThread::ExitThread()
{
	if (!m_thread)
		return;

	// Create a new ThreadMsg
	ThreadMsg* threadMsg = new ThreadMsg(MSG_EXIT_THREAD, 0);

	// Put exit thread message into the queue
	{
		lock_guard<mutex> lock(m_mutex);
		m_queue.push(threadMsg);
		m_cv.notify_one();
	}

	m_thread->join();
	delete m_thread;
	m_thread = 0;
}

//----------------------------------------------------------------------------
// PostMsg
//----------------------------------------------------------------------------
void WorkerThread::PostMsg(const void* data)
{
	ASSERT_TRUE(m_thread);

	ThreadMsg* threadMsg = new ThreadMsg(MSG_POST_USER_DATA, data);
	
	// Add user data msg to queue and notify worker thread
	std::unique_lock<std::mutex> lk(m_mutex);
	m_queue.push(threadMsg);
	m_cv.notify_one();
}

//----------------------------------------------------------------------------
// PostMsg
//----------------------------------------------------------------------------
int WorkerThread::PostMsg(const void* idata, const long& rel_time_ms, void* odata, const unsigned long& osize)
{
	ASSERT_TRUE(m_thread);

	ThreadMsg* threadMsg = new ThreadMsg(MSG_POST_USER_DATA, idata, osize);
	
	// Add user data msg to queue and notify worker thread
	std::unique_lock<std::mutex> lk(m_mutex);
	m_queue.push(threadMsg);
	lk.unlock(); // explicit unlock
	m_cv.notify_one();

	// Wait for event being processed
	int rv = -1;
	try {
		// threadMsg could be potentially deleted
		std::unique_lock<std::mutex> lk_ret(threadMsg->mx);
		cv_status status = cv_status::no_timeout;
		
		while (!threadMsg->finished && (status==cv_status::no_timeout))
			status = threadMsg->cv.wait_for(lk_ret, rel_time_ms * 1ms);
		
		if (threadMsg->finished && (status==cv_status::no_timeout)) {
			// Copy output data
			if((osize>0) && (threadMsg->osize>0) && odata && threadMsg->omsg_)
				memcpy(odata, threadMsg->omsg_, threadMsg->osize);
			// Copy return value
			rv = threadMsg->rv;
		}
		
		// signal back we do not need the return value anymore
		threadMsg->needrv = false;
		lk_ret.unlock(); // explicit unlock
		threadMsg->cv.notify_one();
		
	} catch (exception& e) {
		cerr << "(" << GetCurrentThreadId() << ") Exception while waiting for Event done." << endl;
	}
	
	// threadMsg is always deleted by Process method
	return rv;
}

//----------------------------------------------------------------------------                                                                                                                                                               
// Empty                                                                                                                                                                                                                                    
//----------------------------------------------------------------------------                                                                                                                                                               
bool WorkerThread::Empty()
{
        ASSERT_TRUE(m_thread);

        // Get message queueu empty status                                                                                                                                                                                
        std::unique_lock<std::mutex> lk(m_mutex);
        bool status = m_queue.empty();
	m_cv.notify_one();
	return status;
}


//----------------------------------------------------------------------------
// TimerThread
//----------------------------------------------------------------------------
void WorkerThread::TimerThread()
{
	while (!m_timerExit)
	{
		// Sleep for 250ms then put a MSG_TIMER message into queue
		std::this_thread::sleep_for(250ms);
		
		ThreadMsg* threadMsg = new ThreadMsg(MSG_TIMER, 0);

		// Add timer msg to queue and notify worker thread
		std::unique_lock<std::mutex> lk(m_mutex);
		m_queue.push(threadMsg);
		m_cv.notify_one();
	}
}

//----------------------------------------------------------------------------
// DeleteMsg
//----------------------------------------------------------------------------
void WorkerThread::DeleteMsg(ThreadMsg* threadMsg, int rv) {
	
	// if requested, signal return value
	if(threadMsg->needrv) {
		// set return value
		{
			std::unique_lock<std::mutex> lk(threadMsg->mx);
			threadMsg->rv = rv;		
			threadMsg->finished = true;
			lk.unlock(); // explicit unlock
			threadMsg->cv.notify_one();
		}
		
		// wait for signal return value not needed anymore
		{
			std::unique_lock<std::mutex> lk(threadMsg->mx);
			cv_status status = cv_status::no_timeout;
			
			while(threadMsg->needrv && (status==cv_status::no_timeout))
				status = threadMsg->cv.wait_for(lk, 100ms);
			
			// deallocate space for handler output
			if(threadMsg->omsg_) {
				delete [] (char*)threadMsg->omsg_;
				threadMsg->omsg_ = NULL;
			}
		}
	}
	
	delete threadMsg;
}

//----------------------------------------------------------------------------
// Process
//----------------------------------------------------------------------------
void WorkerThread::Process()
{
	m_timerExit = false;
	std::thread timerThread(&WorkerThread::TimerThread, this);

	while (1)
	{
		ThreadMsg* msg = 0;
		{
			// Wait for a message to be added to the queue
			std::unique_lock<std::mutex> lk(m_mutex);
			while (m_queue.empty())
				m_cv.wait(lk);

			if (m_queue.empty())
				continue;

			msg = m_queue.front();
			m_queue.pop();
		}

		switch (msg->id)
		{
			case MSG_POST_USER_DATA:
			{
				ASSERT_TRUE(msg->msg != NULL);

				int rv = 0;
				
				// Allocate space for handler output
				msg->omsg_ = (msg->osize>0) ? new char[msg->osize] : NULL;
				
				// Call msg handler
				if(m_postHandler)
					rv = m_postHandler(msg->msg, msg->omsg_);
				
				// Delete dynamic data passed through message queue
				DeleteMsg(msg, rv);
				break;
			}

			case MSG_TIMER:
			{  
				// Call msg handler
				if(m_timerHandler)
					m_timerHandler(msg->msg);
				
				DeleteMsg(msg);
				break;
			}
			
			case MSG_EXIT_THREAD:
			{
				m_timerExit = true;
				timerThread.join();

				delete msg;
				std::unique_lock<std::mutex> lk(m_mutex);
				while (!m_queue.empty())
				{
					msg = m_queue.front();
					m_queue.pop();
					DeleteMsg(msg);
				}

				cout << "Exit thread on " << THREAD_NAME << " (" << GetCurrentThreadId() << ")" << endl;
				return;
			}

			default:
				ASSERT();
		}
	}
}


