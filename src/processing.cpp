#include "processing.h"
#include "Log.h"
#include <cmath>

using namespace std;

int new_frame_data(frame_data_t & data, const roi_settings_t rois[])
{
  data.frame_sz = DET_FRAME_SZ;
  data.height = DET_HEIGHT;
  data.width = DET_WIDTH;

  try {
    data.frame    = new FRAME_DATA_TYPE[DET_FRAME_SZ]();
    data.filtered = new FRAME_DATA_TYPE[DET_FRAME_SZ]();
    data.bin8     = new FRAME_DATA_TYPE[DET_FRAME_SZ/(8*8)]();
    data.bin16    = new FRAME_DATA_TYPE[DET_FRAME_SZ/(16*16)]();
    data.bin32    = new FRAME_DATA_TYPE[DET_FRAME_SZ/(32*32)]();

    for(size_t iroi; iroi<DET_NROIS; iroi++) {
      data.col_hist[iroi] = (!rois[iroi].is_empty() && rois[iroi].width>0) ? new uint8_t[rois[iroi].width]() : NULL;
      data.row_hist[iroi] = (!rois[iroi].is_empty() && rois[iroi].height>0) ? new uint8_t[rois[iroi].height]() : NULL;
      data.en_hist[iroi]  = (!rois[iroi].is_empty() && rois[iroi].height>0 && rois[iroi].width>0) ? new uint16_t[DET_ENERGY_NBINS]() : NULL;
    }
  } catch (exception & e) {
    write_log(LOG_ERROR, "Error when allocating memory for processing data");
    return 1;
  }
  
  return 0;
}

int clear_frame_data_postprocess(frame_data_t & data, const roi_settings_t rois[])
{
  data.frame_sz = DET_FRAME_SZ;
  data.height = DET_HEIGHT;
  data.width = DET_WIDTH;

  try {
    FRAME_DATA_TYPE * p = NULL;

    p = data.bin8;
    for(size_t i=0; i<(DET_FRAME_SZ/(8*8)); i++) {
      *p = 0;
      p++;
    }
    
    p =	data.bin16;
    for(size_t i=0; i<(DET_FRAME_SZ/(16*16)); i++) {
      *p = 0;
      p++;
    }

    p =	data.bin32;
    for(size_t i=0; i<(DET_FRAME_SZ/(32*32)); i++) {
      *p = 0;
      p++;
    }

    for(size_t iroi; iroi<DET_NROIS; iroi++) {

      if(!rois[iroi].is_empty() && (rois[iroi].width>0)) {
        uint8_t * p = data.col_hist[iroi];
	for(size_t i=0; i<rois[iroi].width; i++) {
         *p = 0;
         p++;
        }
      }

      if(!rois[iroi].is_empty() && (rois[iroi].height>0)) {
        uint8_t * p = data.row_hist[iroi];
	for(size_t i=0; i<rois[iroi].height; i++) {
         *p = 0;
         p++;
        }
      }

      if(!rois[iroi].is_empty() && (rois[iroi].height>0) && (rois[iroi].width>0)) {
        uint16_t * p = data.en_hist[iroi];
        for(size_t i=0; i<DET_ENERGY_NBINS; i++) {
         *p = 0;
         p++;
        }
      }
    }
  } catch (exception & e) {
    write_log(LOG_ERROR, "Error when setting memory for processing data");
    return 1;
  }

  return 0;
}

int delete_frame_data(frame_data_t & data)
{
  try {
    if(data.frame) {
      delete [] data.frame; data.frame = NULL;
    }
    if(data.filtered) {
      delete [] data.filtered; data.filtered = NULL;
    }
    if(data.bin8) {
      delete [] data.bin8; data.bin8 = NULL;
    }
    if(data.bin16) {
      delete [] data.bin16; data.bin16 = NULL;
    }
    if(data.bin32) {
      delete [] data.bin32; data.bin32 = NULL;
    }

    for(size_t iroi; iroi<DET_NROIS; iroi++) {
      if(data.col_hist[iroi]) {
	delete [] data.col_hist[iroi]; data.col_hist[iroi] = NULL;
      }
      if(data.row_hist[iroi]) {
        delete [] data.row_hist[iroi]; data.row_hist[iroi] = NULL;
      }
      if(data.en_hist[iroi]) {
        delete [] data.en_hist[iroi]; data.en_hist[iroi] = NULL;
      }
    }
  } catch (exception & e) {
    write_log(LOG_ERROR, "Error when releasinge memory for processing data");
    return 1;
  }
  
  return 0;
}

int savefile_frame_data(const frame_data_t & data, const char * dirname, uint16_t outputform, int iframe)
{
  string filename;
  char sbuf[32];

  int ierr = 0;

  snprintf(sbuf, sizeof(sbuf), "%04d", iframe);
  
  // --- images ---
  filename = string(dirname) + string("/frame_") + string(sbuf) + string(".dat");
  ierr += save_file_bin(data.frame,    (outputform & 0x01) ? data.frame_sz : 0, filename.c_str());

  filename = string(dirname) + string("/filtered_") + string(sbuf) + string(".dat");
  ierr += save_file_bin(data.filtered, (outputform & 0x01) ? data.frame_sz : 0, filename.c_str());

  filename = string(dirname) + string("/bin8_") + string(sbuf) + string(".dat");
  ierr += save_file_bin(data.bin8,     (outputform & 0x02) ? (data.frame_sz/(8*8)) : 0, filename.c_str());

  filename = string(dirname) + string("/bin16_") + string(sbuf) + string(".dat");
  ierr += save_file_bin(data.bin16,    (outputform & 0x04) ? (data.frame_sz/(16*16)) : 0, filename.c_str());

  filename = string(dirname) + string("/bin32_") + string(sbuf) + string(".dat");
  ierr += save_file_bin(data.bin32,    (outputform & 0x08) ? (data.frame_sz/(32*32)) : 0, filename.c_str());
  
  return ierr;
}

int savefile_histograms(const frame_data_t & data, const char * dirname, const experiment_metainfo_t & info, uint16_t outputform, int iframe) {

  string filename;
  int ierr = 0;
  char sbuf[32];
  char sbuf2[32];
  
  for(size_t iroi=0; iroi<DET_NROIS; iroi++) {
    snprintf(sbuf , sizeof(sbuf ), "%ld", iroi);
    snprintf(sbuf2, sizeof(sbuf2), "%04d", iframe);
    if(outputform & XRAY_OUTPUTFORM_HIST) {
      // row
      filename = string(dirname) + string("/row_histogram_") + string(sbuf2) + string("_roi") + string(sbuf) + string(".dat");
      ierr += save_file_bin(data.row_hist[iroi], (data.row_hist[iroi]) ? info.rois[iroi][2] : 0, filename.c_str());
      // col
      filename = string(dirname) + string("/col_histogram_") + string(sbuf2) + string("_roi") + string(sbuf) + string(".dat");
      ierr += save_file_bin(data.col_hist[iroi], (data.col_hist[iroi]) ? info.rois[iroi][3] : 0, filename.c_str());
    }
    if(outputform & XRAY_OUTPUTFORM_ENHIST) {
      // energy
      filename = string(dirname) + string("/energy_histogram_") + string(sbuf2) + string("_roi") + string(sbuf) + string(".dat");
      ierr += save_file_bin(data.en_hist[iroi], (data.en_hist[iroi]) ? DET_ENERGY_NBINS : 0, filename.c_str());
    }
  }

  return ierr;
}

int mask_above(FRAME_DATA_TYPE * out, const FRAME_DATA_TYPE * in, size_t len, FRAME_DATA_TYPE threshold)
{
  const FRAME_DATA_TYPE * pin = in;
  FRAME_DATA_TYPE * pout = out;

  for(size_t i=0; i<len; i++) {
    const FRAME_DATA_TYPE val = *pin;
    *pout = (val>threshold) ? 0 : val;
    pin++;
    pout++;
  }

  return 0;
}

int filter_single_events(FRAME_DATA_TYPE * filtered, const FRAME_DATA_TYPE * frame, size_t height, size_t width)
{   
  FRAME_DATA_TYPE tl;    // top-left
  FRAME_DATA_TYPE tc;    // top-current
  FRAME_DATA_TYPE tr;    // top-right
  FRAME_DATA_TYPE cl;    // current-left
  FRAME_DATA_TYPE cc;    // current-current
  FRAME_DATA_TYPE cr;    // current-right
  FRAME_DATA_TYPE bl;    // botton-left
  FRAME_DATA_TYPE bc;    // botton-curent
  FRAME_DATA_TYPE br;    // botton-right

  const FRAME_DATA_TYPE * pcurr = frame; // pointer to current-right
  const FRAME_DATA_TYPE * ptopr = frame; // pointer to top-right (starts moving at the end of the 1st line)
  const FRAME_DATA_TYPE * pbotr = frame + width; // pointer to bottom-right 

  FRAME_DATA_TYPE * p = filtered;

  tr = 0;
  cr = *pcurr; // will become current-current
  pcurr++;
  br = *pbotr;
  pbotr++;
  
  for(size_t irow=0; irow<height; irow++) {
    
    bool notfirstrow = (irow > 0);
    bool notlastrow = (irow != (height-1));

    // set registers at the begining of a row
    tl = 0; tc = tr; tr = (notfirstrow) ? *ptopr : 0;
    cl = 0; cc = cr; cr = *pcurr;
    bl = 0; bc = (notlastrow) ? br : 0; br = (notlastrow) ? *pbotr : 0;
    
    for(size_t icol=0; icol<(width-1); icol++) {
      // filter
      if((cc>0) && (cl==0) && (cr==0) && (tl==0) && (tc==0) && (tr==0) && (bl==0) && (bc==0) && (br==0))
	*p = cc;
      else
	*p = 0;
      // shift
      p++;
      if(notfirstrow) ptopr++;
      pcurr++;
      if(notlastrow) pbotr++;
      // shift values
      tl = tc; tc = tr; tr = (notfirstrow) ? *ptopr : 0;
      cl = cc; cc = cr; cr = *pcurr;
      bl = bc; bc = br;	br = (notlastrow) ? *pbotr : 0; 
    }

    // filter the last column 
    if((cc>0) && (cl==0) && (tl==0) && (tc==0) && (bl==0) && (bc==0))
      *p = cc;
    else
      *p = 0;
    // take top-right value before shift
    tr = *ptopr;   
    // shift                                                                                                                                                                                                                                
    p++;
    ptopr++;
    pcurr++;
    if(notlastrow) pbotr++;
  }

  return 0;
}

void calc_binning_no_gap_no_hot(frame_data_t & data, uint16_t outputform)
{
  const FRAME_DATA_TYPE * p = data.filtered;

  size_t w8  = data.width/8;
  size_t w16 = data.width/16;
  size_t w32 = data.width/32;

  for(size_t irow=0; irow<data.height; irow++) {
    size_t ioff8  = (irow/8)  * w8;
    size_t ioff16 = (irow/16) * w16;
    size_t ioff32 = (irow/32) * w32;
    for(size_t icol=0; icol<data.width; icol++) {
      FRAME_DATA_TYPE val = *p;
      size_t ic8  = icol/8;
      size_t ic16 = icol/16;
      size_t ic32 = icol/32;
      if(outputform & XRAY_OUTPUTFORM_BIN_8) {
	FRAME_DATA_TYPE s = data.bin8[ioff8 + ic8];
        if(val<=(std::numeric_limits<FRAME_DATA_TYPE>::max()-s))
          data.bin8[ioff8 + ic8] = s + val;
        else
          data.bin8[ioff8 + ic8] = std::numeric_limits<FRAME_DATA_TYPE>::max();
      }
      if(outputform & XRAY_OUTPUTFORM_BIN_16) {
        FRAME_DATA_TYPE s = data.bin16[ioff16 + ic16];
        if(val<=(std::numeric_limits<FRAME_DATA_TYPE>::max()-s))
          data.bin16[ioff16 + ic16] = s + val;
        else
          data.bin16[ioff16 + ic16] = std::numeric_limits<FRAME_DATA_TYPE>::max();
      }
      if(outputform & XRAY_OUTPUTFORM_BIN_32) {
        FRAME_DATA_TYPE s = data.bin32[ioff32 + ic32];
        if(val<=(std::numeric_limits<FRAME_DATA_TYPE>::max()-s))
          data.bin32[ioff32 + ic32] = s + val;
        else
          data.bin32[ioff32 + ic32] = std::numeric_limits<FRAME_DATA_TYPE>::max();
      }
      
      p++;
      
    } // icol                                                                                                                                                                                                                                
  } // irow                                                                                                                                                                                                                                  
}

int mask_out_of_roi(FRAME_DATA_TYPE * out, const FRAME_DATA_TYPE * in, size_t height, size_t width, const roi_settings_t & roi)
{
  const FRAME_DATA_TYPE * pin = in;
  FRAME_DATA_TYPE * pout = out;
	  
  for(size_t irow=0; irow<height; irow++) {
    for(size_t icol=0; icol<width; icol++) {
      if((icol>=roi.col) && (irow>=roi.row) && (icol<(roi.col+roi.width)) && (irow<(roi.row+roi.height)))
	*pout = *pin;
      else
	*pout = (FRAME_DATA_TYPE)0;
      pin++;
      pout++;
    } // icol
  } // irow

  return 0;
}

float tot_to_energy(size_t ipix, FRAME_DATA_TYPE tot, bool use_calib)
{
  if(tot==0)
    return 0.0;
  else
    if(use_calib && g_det_calib_avail) {
      float a  = g_det_caliba[ipix];
      float t_ = g_det_calibt[ipix];
      float b_ = g_det_calibb[ipix];
      float b = b_ - a*t_ - tot;
      float c = (tot-b_)*t_ - g_det_calibc[ipix];
      // a==0
      if(a==0.0)
	if(b!=0.0)
	  return (-c/b); // a==0
	else
	  return 0.0; // a==0, b==0
      else // a!=0
	if(b==0.0)
	  if(c<0.0)
	    return sqrt(-c/abs(a));
	  else
	    return 0.0;
	else { // a!=0, b!=0
	  float q = b*b - 4.0*a*c;
	  if(q>0.0) {
	    q = -0.5*(b + ((b>0.) ? 1 : (-1))*sqrt(q));
	    float x1 = q/a;
	    float x2 = c/q;
	    if((x1>0.0) || (x2>0.0))
	      // return the larger one
	      return (x2>x1) ? x2 : x1;
	    else
	      return 0.0;
	  } else if (q==0.0) {
	    float x = -b/(2.0*a);
	    return (x>=0.0) ? x : 0.0;
	  } else
	    return 0.0; // determinant<0.
	}
    } else
      return (float)tot;
}

FRAME_DATA_TYPE energy_to_tot(size_t ipix, float en, bool use_calib)
{
  if(use_calib && g_det_calib_avail)
    if(en==0.0)
      return 0;
    else {
      float t = g_det_calibt[ipix];
      if(en<=t)
	return 0;
      else {
	float tot = g_det_caliba[ipix]*en + g_det_calibb[ipix]; // a*E+b
	float c = g_det_calibc[ipix];
	if(c!=0.0) {
	  t = en - t; // E-t
	  if(t!=0.0)
	    tot += -c/t; // -c/(E-t)
	  else
	    return 0;
	}
	if(tot>=std::numeric_limits<FRAME_DATA_TYPE>::max())
	  return std::numeric_limits<FRAME_DATA_TYPE>::max();
	else
	  return (FRAME_DATA_TYPE)(tot+0.5);
      }
    }
  else 
    return (FRAME_DATA_TYPE)(en+0.5);
}
