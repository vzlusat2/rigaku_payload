
/**
 * CSP-Terminal
 *
 * @author Johan De Claville Christiansen
 * Copyright 2010-2013 GomSpace ApS. All rights reserved.
 * 
 * updated sabol@vzlu.cz
 */


// exit declaration
#include <stdlib.h>
#include <stdio.h>

/* CSP */
#include <csp/csp.h>
#include <csp/csp_cmp.h>
#include <csp/csp_endian.h>
#include <csp/arch/csp_clock.h>
#include <csp/arch/csp_thread.h>
#include <csp/drivers/can_socketcan.h>
#include <csp/arch/csp_time.h>

#if (CSP_POSIX)
	#include <unistd.h>
        #include "posix/tcpbridge.h"
extern "C" {
        #include "posix/tcpcli.h"
}
#endif // CSP_POSIX

#include "cspTask.h"
#include "Log.h"
#include "xray.h"
#include "version.h"
#include "pxcapi.h"

#define NODE_RITE    (NODE_XRAY)

#define NODE_COMM    5
#define NODE_CSPEMU  20

// since epoch
csp_timestamp_t ts;
uint32_t seconds_last = 0;
uint32_t millisec_last = 0;

void csp_clock_get_time(csp_timestamp_t * time) {
  uint32_t seconds_now = csp_get_s();
  uint32_t millisec_now = csp_get_ms();
  // add time offset since last call
  uint32_t sec =  (millisec_now - millisec_last) / 1000;
  uint32_t msec = (millisec_now - millisec_last) % 1000 + ts.tv_nsec/1000000;
  if(msec>=1000) {
    sec++;
    msec -= 1000;
  }
  ts.tv_sec += sec;
  ts.tv_nsec = msec*1000000;
  time->tv_sec = ts.tv_sec ;
  time->tv_nsec = ts.tv_nsec;
  seconds_last = seconds_now;
  millisec_last = millisec_now;
}

int csp_clock_set_time(const csp_timestamp_t * time) {
  ts.tv_sec = time->tv_sec;
  ts.tv_nsec = time->tv_nsec;
  // last synchronization stamp
  seconds_last = csp_get_s();
  millisec_last = csp_get_ms();
  return 0;
}
  
void print_help(void) {
	printf("  -a ADDRESS,\tSet address (default: 8)\r\n");
	printf("  -c DEVICE,\tSet can device (default: can0)\r\n");
	printf("  -s SPEED,\tSet can speed (default: 1000000 b/s)\r\n");
	printf("  -t IPADDRESS,\tSet default interface to TCP client and connects to IPADDRESS:9026\r\n");
	printf("  -p ADDRESS,\tPing node\r\n");
	printf("  -v ADDRESS,\tVia to route\r\n");
	printf("  -e,\t\tEnable debug info\r\n");
	printf("  -h,\t\tPrint help and exit\r\n");
}

void csp_assert_fail_action(const char *assertion, const char *file, int line) {
	printf("csp_assert_fail_action\r\n");
	exit(EXIT_FAILURE);
}

void exithandler(void)
{
    // Shutdown threads
    //tcpbridge_done();
    tcpcli_done();
    // nothing to do
    int rc = pxcExit();
    if (rc) {
      write_log(LOG_ERROR, "Error when deinitializing Pixet, error: %d\n", rc);
    }
}

int main(int argc, char * argv[]) {

	atexit(exithandler);

	/** CAN STUFF */
	std::string ifc = "can0";
	int bitrate = 1000000;
	uint8_t can = 1;
	uint8_t addr = NODE_RITE;
	uint8_t route_via = NODE_OBC;
	int ping_addr = -1;
	std::string ipadress = "127.0.0.1";
	
	// enable Error by default
	csp_debug_set_level(CSP_ERROR, true);

#if (CSP_POSIX)
	/** Parser */
	int c;
	while ((c = getopt(argc, argv, "a:c:s:t:p:v:eht")) != -1) {
		switch (c) {
		case 'a':
			addr = atoi(optarg);
			break;
		case 'c':
			can = 1;
			ifc = optarg;
			break;
		case 's':
			bitrate = atoi(optarg);
			break;
		case 't':
		        can = 0;
		        ipadress = optarg;
			break;
		case 'p':
			ping_addr = atoi(optarg);
			break;
		case 'v':
			route_via = atoi(optarg);
		break;
		case 'e':
			csp_debug_set_level(CSP_INFO, true);
		break;
		case 'h':
			print_help();
			exit(0);
		case '?':
			return 1;
		default:
			exit(EXIT_FAILURE);
		}
	}
#endif // CSP_POSIX


    char version_str[64];
    snprintf(version_str, sizeof(version_str), "%d", APP_VERSION);
      
	csp_conf_t conf;
    csp_conf_get_defaults(&conf);
    conf.hostname = "RITE";
    conf.model = "Mars ZX2";
    conf.revision = version_str;
    conf.buffers = 400;
    conf.buffer_data_size = 512;
    conf.address = addr;

	int rv = csp_init(&conf);
    if (CSP_ERR_NONE != rv) {
        fprintf(stderr, "CSP init error %d\n", rv);
        exit(EXIT_FAILURE);
    }

	csp_rdp_set_opt(6, 30000, 16000, 1, 8000, 3);

	csp_iface_t * csp_if_can = NULL;
	if (can == 1) {
	  if ((csp_if_can = csp_can_socketcan_init(ifc.c_str(), bitrate, false)) == NULL) {
            fprintf(stderr, "failed to add CAN interface [%s]", ifc.c_str());
            exit(1);
          }

	  // explicit routing table (Sep 01)
	  csp_rtable_set(0, 1, csp_if_can, CSP_NODE_MAC);
	  csp_rtable_set(0, 0, csp_if_can, NODE_COMM);
	  csp_rtable_set(NODE_CSPEMU, 3, csp_if_can, NODE_CSPEMU);
 
	} else {
	  /* Configure TCP client and set it as the default interface */
	  tcpcli_init();
	  if (tcpcli_start(ipadress.c_str()) != 0) {
	    fprintf(stderr, "failed to add TCPCLI interface");
	    exit(EXIT_FAILURE);
	  }

	  csp_route_set(CSP_DEFAULT_ROUTE, &csp_if_tcpcli, route_via);
	}
	
	/* Router */
	csp_route_start_task(1000, 0);

	if (ping_addr != -1)
	{
		int result = csp_ping(ping_addr, 1000, 100, CSP_O_NONE);
		printf("Ping result %d [ms]\r\n", result);
	}

	/* Synchronize time with via-node */
	synchronize_time(route_via, true, true);

	int rc = pxcInitialize();
	if (rc) {
	  write_log(LOG_ERROR, "Could not initialize Pixet, error: %d\n", rc);
	}

	/* Server */
	csp_thread_handle_t handle_server;
	csp_thread_create(cspTask, "SERVER", 0, NULL, 0, &handle_server);

	/* Wait for execution to end (ctrl+c) */
	while(1) {
	  csp_sleep_ms(100000);
	}
	
	rc = pxcExit();
        if (rc) {
          write_log(LOG_ERROR, "Error when deinitializing Pixet, error: %d\n", rc);
        }

	return 0;
}

