/* -------------------------------------------------------------------- */
/*	I/O task						                                              	*/
/* -------------------------------------------------------------------- */

#include <unistd.h>

#include <csp/csp.h>
#include <csp/csp_endian.h>

#include "xray.h"
#include "Log.h"
#include "ioTask.h"
#include "mainTask.h"
#include "cspTask.h"
#include "dkHandler.h"
#include "processing.h"

#define DATA_PACKET_SIZE           64
#define DK_STORAGE_ACK_TIMEOUT   1000

using namespace std;

// --- Global variables -----------------------------
uint32_t g_session_firstChunkId = 0;
uint32_t g_session_lastChunkId = 0;

// --- Forward declaration --------------------------
void saveUint16(uint8_t * buffer, uint16_t value);
int xray_io_send_frame_events(const io_frame_data_t & frame, uint32_t & firstChunkId, uint32_t & lastChunkId);
int xray_io_send_histogram(const io_histogram_data_t & frame, uint32_t & firstChunkId, uint32_t & lastChunkId);
int xray_io_send_energy_histogram(const io_energy_histogram_data_t & hist, uint32_t & firstChunkId, uint32_t & lastChunkId);
int xray_io_send_binned_data(const io_binned_data_t & data, uint32_t & firstChunkId, uint32_t & lastChunkId);
int xray_io_send_metainfo(const experiment_metainfo_t & info, uint32_t & firstChunkId, uint32_t & lastChunkId);
int xray_io_send_expconfig(const experiment_config_t & config);
int xray_io_send_status(const xray_status_t & status);
// --------------------------------------------------

int ioTask(const void * taskParam, void * taskOutput)
{
  // Convert the ThreadMsg void* data to an ioEvent_t*         
  const ioEvent_t* ioEvt = static_cast<const ioEvent_t*>(taskParam);

  uint32_t first, last;
  
  switch(ioEvt->eventType) {
    case XRAY_IO_TASK_VOID: {
      int *prv = (int*) taskOutput;
      *prv = 0;
      break;
    }
    case XRAY_IO_SEND_METAINFO: {
      experiment_metainfo_t *info = (experiment_metainfo_t*) ioEvt->pvData;
      //int *prv = (int*) taskOutput;
      //*prv = xray_detector_init();
      xray_io_send_metainfo(*info, first, last);
      delete info;
      break;
    }
    case XRAY_IO_SEND_CONFIG: {
      experiment_config_t *cfg = (experiment_config_t*) ioEvt->pvData;
      int *prv = (int*) taskOutput;  
      *prv = xray_io_send_expconfig(*cfg);
      delete cfg;
      break;
    }
    case XRAY_IO_SEND_FRAME_EVENTS: {
      io_frame_data_t *frame = (io_frame_data_t*) ioEvt->pvData;
      xray_io_send_frame_events(*frame, first, last);
      delete [] frame->data;
      delete frame;
      break;
    }
    case XRAY_IO_SEND_ENERGY_HISTOGRAM: {
      io_energy_histogram_data_t *hist = (io_energy_histogram_data_t*) ioEvt->pvData;
      xray_io_send_energy_histogram(*hist, first, last);
      delete [] hist->data;
      delete hist;
      break;
    }
    case XRAY_IO_SEND_HISTOGRAMS: {
      io_histogram_data_t *hist = (io_histogram_data_t*) ioEvt->pvData;
      xray_io_send_histogram(*hist, first, last);
      delete [] hist->data;
      delete hist;
      break; 
    }
    case XRAY_IO_SEND_BINNED_DATA: {
      io_binned_data_t *data = (io_binned_data_t*) ioEvt->pvData;
      xray_io_send_binned_data(*data, first, last);
      delete [] data->data;
      delete data;
      break;
    }
    case XRAY_IO_START_SESSION: {
      g_session_firstChunkId = getNextChunkId(STORAGE_DATA_ID);
      break;
    }
    case XRAY_IO_END_SESSION: {
      g_session_lastChunkId = getNextChunkId(STORAGE_DATA_ID) - 1;
      break;
    }
    case XRAY_IO_SYSTEM_SHUTDOWN: {
      xray_system_shutdown();
      break;
    }
    case XRAY_IO_SEND_STATUS: {
      xray_status_t *status = (xray_status_t*) ioEvt->pvData;
      int *prv = (int*) taskOutput;
      *prv = xray_io_send_status(*status);
      delete status;
      break;
    }  
    default: {
      break;
    }
  }
  
  // Delete dynamic data passed through message queue                                                                                                                                                                                        
  delete ioEvt;
  return 0;
}

void saveUint16(uint8_t * buffer, uint16_t value)
{	
  uint8_t * tempPtr = static_cast<uint8_t*>((void*)(&value));
  
  *buffer = *tempPtr;
  *(buffer+1) = *(tempPtr+1);
}

int xray_io_send_frame_events(const io_frame_data_t & frame, uint32_t & firstChunkId, uint32_t & lastChunkId)
{
  const size_t payload_sz = DATA_PACKET_SIZE;

  firstChunkId = getNextChunkId(STORAGE_DATA_ID);
    
  uint8_t buf[sizeof(dk_msg_store_ack_t)+payload_sz];
  dk_msg_store_ack_t * message = (dk_msg_store_ack_t *)buf;
  
  message->parent.cmd = DKC_STORE_ACK;
  message->port = STORAGE_DATA_ID;
  message->host = CSP_DK_MY_ADDRESS;
    
  int ierr = 0;
  
  uint8_t packetPointer, numPixelsInPacket;
  uint16_t tempPixel;
  
  // initialize the first packet
  message->data[0] = (frame.type) ? 'B' : 'b';
  saveUint16(message->data+1, frame.id);
  packetPointer = 4;
  numPixelsInPacket = 0;

  const FRAME_DATA_TYPE * p = frame.data;
  
  for (size_t irow = 0; irow<frame.height; irow++) {
    for (size_t icol = 0; icol<frame.width; icol++) {
      uint16_t tempPixel = *p;
      // the pixel will be send
      if (tempPixel > 0) {
  // there is still a place in the packet
  if (packetPointer <= (DATA_PACKET_SIZE-4)) {
    saveUint16(message->data + packetPointer, irow*frame.width+icol);
    packetPointer += 2;
    saveUint16(message->data + packetPointer, tempPixel);
    packetPointer += 2;
    numPixelsInPacket++;
  }				
  // the packet is full, send it
  if (packetPointer > (DATA_PACKET_SIZE-4)) {			
    message->data[3] = numPixelsInPacket;

    usleep(XRAY_DK_SLEEP_TIME_MS*1000);
      
    int iserr = 0;
    for (size_t k = 0; k < NUMBER_DK_REQ_TRIALS; k++) {
      iserr = 1;                                                                                                                                                                   
      xcsp_buf_sendto(CSP_PRIO_NORM, CSP_DK_ADDRESS, CSP_DK_PORT, XRAY_PORT_DK_COM_ACK, CSP_O_NONE, buf, sizeof(dk_msg_store_ack_t)+packetPointer, 1000);
      if (waitForDkAck(DK_STORAGE_ACK_TIMEOUT) == 1) {
        iserr = 0;
        break;
      }
    }
    if(iserr)
      ierr++;

    usleep(XRAY_DK_SLEEP_TIME_MS*1000);
    
    message->data[0] = (frame.type) ? 'B' : 'b';
    saveUint16(message->data+1, frame.id);
    packetPointer = 4;
    numPixelsInPacket = 0;
  } // when full
      } // pixel > 0
      if(ierr==10) {
  write_log(LOG_ERROR, "To many errors when sending frame data events (id=%d), giving up", frame.id);
  break;
      }
      p++;
    } // icol
    if(ierr==10)
      break;
  } // irow
    
  // send the last data packet
  if ((packetPointer > 4) && (ierr<10)) {
    message->data[3] = numPixelsInPacket;

    usleep(XRAY_DK_SLEEP_TIME_MS*1000);
    
    int iserr = 0;
    for (size_t k = 0; k < NUMBER_DK_REQ_TRIALS; k++) {
      iserr = 1;
      xcsp_buf_sendto(CSP_PRIO_NORM, CSP_DK_ADDRESS, CSP_DK_PORT, XRAY_PORT_DK_COM_ACK, CSP_O_NONE, buf, sizeof(dk_msg_store_ack_t)+packetPointer, 1000);
      if (waitForDkAck(DK_STORAGE_ACK_TIMEOUT) == 1) {
  iserr = 0;
  break;
      }
    }
    if(iserr)
      ierr++;

    usleep(XRAY_DK_SLEEP_TIME_MS*1000);
  }

  lastChunkId = getNextChunkId(STORAGE_DATA_ID) - 1;
  
  write_log(LOG_INFO_L2, "Frame events sent, firstChunk=%u, lastChunk=%u", firstChunkId, lastChunkId);
  
  return ierr;
}

uint32_t getNextChunkId(uint8_t port) {

  uint8_t buf[sizeof(dk_msg_storage_t)];
  dk_msg_storage_t * message = (dk_msg_storage_t *)buf;
  
  message->host = CSP_DK_MY_ADDRESS;
  message->port = port;
  message->parent.cmd = DKC_INFO;
  
  uint32_t chunksNum = 0;

  usleep(XRAY_DK_SLEEP_TIME_MS*1000);
  
  for (size_t i = 0; i<NUMBER_DK_REQ_TRIALS; i++) {
    xcsp_buf_sendto(CSP_PRIO_NORM, CSP_DK_ADDRESS, CSP_DK_PORT, XRAY_PORT_DK_INFO_ACK, CSP_O_NONE, buf, sizeof(dk_msg_storage_t), 1000);
    chunksNum = csp_hton32(waitForDkInfoAck());
    if (chunksNum > 0)		  
      break;
  }

  usleep(XRAY_DK_SLEEP_TIME_MS*1000);
  
  return chunksNum + 1;
}

uint32_t waitForDkInfoAck() {

  uint32_t chunks = 0;

  if (!sCSPDKInfoAckConsumable.consume(chunks, 3s)) { // no consume err
    
    write_log(LOG_INFO_L2, "ack chunks: %u", csp_ntoh32(chunks));

    return chunks;
  }

  write_log(LOG_ERROR, "chunks: timeout");

  return 0;
}

int xray_io_send_histogram(const io_histogram_data_t & hist, uint32_t & firstChunkId, uint32_t & lastChunkId)
{
  const size_t payload_sz = DATA_PACKET_SIZE;

  firstChunkId = getNextChunkId(STORAGE_DATA_ID);
    
  uint8_t buf[sizeof(dk_msg_store_ack_t)+payload_sz];
  dk_msg_store_ack_t * message = (dk_msg_store_ack_t *)buf;
  
  message->parent.cmd = DKC_STORE_ACK;
  message->port = STORAGE_DATA_ID;
  message->host = CSP_DK_MY_ADDRESS;
    
  int ierr = 0;
  
  uint8_t packetPointer, numPointsInPacket;
  
  uint8_t npackets = 0;
  
  // initialize the first packet
  message->data[0] = (hist.type) ? 'H' : 'h';
  saveUint16(message->data+1, hist.id);
  message->data[3] = npackets; // packet number
  message->data[4] = hist.roi_id;
  saveUint16(message->data+5, hist.len);
  packetPointer = 7;
  numPointsInPacket = 0;

  const uint8_t * p = hist.data;
  
  for (size_t i = 0; i<hist.len; i++) {
    if (packetPointer < DATA_PACKET_SIZE) {
      message->data[packetPointer] = *p;
      numPointsInPacket++;
      packetPointer++;
    }
    // the packet is full, send it
    if (packetPointer >= DATA_PACKET_SIZE) {	
          
      message->data[3] = npackets;
      
      usleep(XRAY_DK_SLEEP_TIME_MS*1000);
      
      int iserr = 0;
      for (size_t k = 0; k < NUMBER_DK_REQ_TRIALS; k++) {
        iserr = 1;                                                                                                                                                                   
        xcsp_buf_sendto(CSP_PRIO_NORM, CSP_DK_ADDRESS, CSP_DK_PORT, XRAY_PORT_DK_COM_ACK, CSP_O_NONE, buf, sizeof(dk_msg_store_ack_t)+packetPointer, 1000);
        if (waitForDkAck(DK_STORAGE_ACK_TIMEOUT) == 1) {
          iserr = 0;
          break;
        }
      }
      if(iserr)
        ierr++;

      usleep(XRAY_DK_SLEEP_TIME_MS*1000);
    
      npackets++;
      
      message->data[0] = (hist.type) ? 'H' : 'h';
      saveUint16(message->data+1, hist.id);
      message->data[3] = npackets; // packet number
      message->data[4] = hist.roi_id;
      saveUint16(message->data+5, hist.len);
      packetPointer = 7;
      numPointsInPacket = 0;
    } // when full
    if(ierr==10) {
      write_log(LOG_ERROR, "To many errors when sending histogram data events (id=%d), giving up", hist.id);
      break;
    }
    p++;
  } // i
    
  // send the last data packet
  if ((packetPointer > 7) && (ierr<10)) {
    
    message->data[3] = npackets;

    usleep(XRAY_DK_SLEEP_TIME_MS*1000);
    
    int iserr = 0;
    for (size_t k = 0; k < NUMBER_DK_REQ_TRIALS; k++) {
      iserr = 1;
      xcsp_buf_sendto(CSP_PRIO_NORM, CSP_DK_ADDRESS, CSP_DK_PORT, XRAY_PORT_DK_COM_ACK, CSP_O_NONE, buf, sizeof(dk_msg_store_ack_t)+packetPointer, 1000);
      if (waitForDkAck(DK_STORAGE_ACK_TIMEOUT) == 1) {
        iserr = 0;
        break;
      }
    }
    if(iserr)
      ierr++;

    usleep(XRAY_DK_SLEEP_TIME_MS*1000);
  }

  lastChunkId = getNextChunkId(STORAGE_DATA_ID) - 1;
  
  write_log(LOG_INFO_L2, "Histogram sent (id=%u,type=%c,roi=%u), firstChunk=%u, lastChunk=%u", hist.id, (hist.type) ? 'H' : 'h', hist.roi_id, firstChunkId, lastChunkId);
  
  return ierr;
}

int xray_io_send_energy_histogram(const io_energy_histogram_data_t & hist, uint32_t & firstChunkId, uint32_t & lastChunkId)
{
  const size_t payload_sz = DATA_PACKET_SIZE;

  firstChunkId = getNextChunkId(STORAGE_DATA_ID);
    
  uint8_t buf[sizeof(dk_msg_store_ack_t)+payload_sz];
  dk_msg_store_ack_t * message = (dk_msg_store_ack_t *)buf;
  
  message->parent.cmd = DKC_STORE_ACK;
  message->port = STORAGE_DATA_ID;
  message->host = CSP_DK_MY_ADDRESS;
    
  int ierr = 0;
  
  uint8_t packetPointer, numPointsInPacket;
  
  uint8_t npackets = 0;
  
  // initialize the first packet
  message->data[0] = 'e';
  saveUint16(message->data+1, hist.id);
  message->data[3] = npackets; // packet number
  message->data[4] = hist.roi_id;
  saveUint16(message->data+5, hist.en_max);
  packetPointer = 7;
  numPointsInPacket = 0;

  const uint16_t * p = hist.data;
  
  for (size_t i = 0; i<DET_ENERGY_NBINS; i++) {
    if (packetPointer <= (DATA_PACKET_SIZE-2)) {
      saveUint16(message->data + packetPointer, *p);
      packetPointer += 2;
      numPointsInPacket++;
    }
    // the packet is full, send it
    if (packetPointer >= DATA_PACKET_SIZE) {	
          
      message->data[3] = npackets;
      
      usleep(XRAY_DK_SLEEP_TIME_MS*1000);
      
      int iserr = 0;
      for (size_t k = 0; k < NUMBER_DK_REQ_TRIALS; k++) {
        iserr = 1;                                                                                                                                                                   
        xcsp_buf_sendto(CSP_PRIO_NORM, CSP_DK_ADDRESS, CSP_DK_PORT, XRAY_PORT_DK_COM_ACK, CSP_O_NONE, buf, sizeof(dk_msg_store_ack_t)+packetPointer, 1000);
        if (waitForDkAck(DK_STORAGE_ACK_TIMEOUT) == 1) {
          iserr = 0;
          break;
        }
      }
      if(iserr)
        ierr++;

      usleep(XRAY_DK_SLEEP_TIME_MS*1000);
    
      npackets++;
      
      message->data[0] = 'e';
      saveUint16(message->data+1, hist.id);
      message->data[3] = npackets; // packet number
      message->data[4] = hist.roi_id;
      saveUint16(message->data+5, hist.en_max);
      packetPointer = 7;
      numPointsInPacket = 0;
    } // when full
    if(ierr==10) {
      write_log(LOG_ERROR, "To many errors when sending energy histogram data events (id=%d), giving up", hist.id);
      break;
    }
    p++;
  } // i
    
  // send the last data packet
  if ((packetPointer > 7) && (ierr<10)) {
    
    message->data[3] = npackets;

    usleep(XRAY_DK_SLEEP_TIME_MS*1000);
    
    int iserr = 0;
    for (size_t k = 0; k < NUMBER_DK_REQ_TRIALS; k++) {
      iserr = 1;
      xcsp_buf_sendto(CSP_PRIO_NORM, CSP_DK_ADDRESS, CSP_DK_PORT, XRAY_PORT_DK_COM_ACK, CSP_O_NONE, buf, sizeof(dk_msg_store_ack_t)+packetPointer, 1000);
      if (waitForDkAck(DK_STORAGE_ACK_TIMEOUT) == 1) {
        iserr = 0;
        break;
      }
    }
    if(iserr)
      ierr++;

    usleep(XRAY_DK_SLEEP_TIME_MS*1000);
  }

  lastChunkId = getNextChunkId(STORAGE_DATA_ID) - 1;
  
  write_log(LOG_INFO_L2, "Energy histogram sent (id=%u,roi=%u,en_max=%u), firstChunk=%u, lastChunk=%u", hist.id, hist.roi_id, hist.en_max, firstChunkId, lastChunkId);
  
  return ierr;
}

int xray_io_send_binned_data(const io_binned_data_t & data, uint32_t & firstChunkId, uint32_t & lastChunkId)
{
  const size_t payload_sz = DATA_PACKET_SIZE;

  firstChunkId = getNextChunkId(STORAGE_DATA_ID);
    
  uint8_t buf[sizeof(dk_msg_store_ack_t)+payload_sz];
  dk_msg_store_ack_t * message = (dk_msg_store_ack_t *)buf;
  
  message->parent.cmd = DKC_STORE_ACK;
  message->port = STORAGE_DATA_ID;
  message->host = CSP_DK_MY_ADDRESS;
    
  int ierr = 0;
  
  uint8_t packetPointer, numPointsInPacket;
  
  uint8_t npackets = 0;
  
  char outId = 'X';
  // initialize the first packet
  if (data.type == XRAY_OUTPUTFORM_BIN_8)
    outId = 'D';
  else if (data.type == XRAY_OUTPUTFORM_BIN_16)
    outId = 'E';
  else if (data.type == XRAY_OUTPUTFORM_BIN_32)
    outId = 'F';
  
  message->data[0] = outId;
  saveUint16(message->data+1, data.id);
  message->data[3] = npackets; // packet number
  packetPointer = 4;
  numPointsInPacket = 0;

  const uint16_t * p = data.data;
  
  for (size_t i = 0; i<data.len; i++) {
    if (packetPointer <= (DATA_PACKET_SIZE-2)) {
      saveUint16(message->data + packetPointer, *p);
      packetPointer += 2;
      numPointsInPacket++;
    }
    // the packet is full, send it
    if (packetPointer >= DATA_PACKET_SIZE) {	
          
      message->data[3] = npackets;
      
      usleep(XRAY_DK_SLEEP_TIME_MS*1000);
      
      int iserr = 0;
      for (size_t k = 0; k < NUMBER_DK_REQ_TRIALS; k++) {
        iserr = 1;                                                                                                                                                                   
        xcsp_buf_sendto(CSP_PRIO_NORM, CSP_DK_ADDRESS, CSP_DK_PORT, XRAY_PORT_DK_COM_ACK, CSP_O_NONE, buf, sizeof(dk_msg_store_ack_t)+packetPointer, 1000);
        if (waitForDkAck(DK_STORAGE_ACK_TIMEOUT) == 1) {
          iserr = 0;
          break;
        }
      }
      if(iserr)
        ierr++;

      usleep(XRAY_DK_SLEEP_TIME_MS*1000);
    
      npackets++;

      message->data[0] = outId;
      saveUint16(message->data+1, data.id);
      message->data[3] = npackets; // packet number
      packetPointer = 4;
      numPointsInPacket = 0;
    } // when full
    if(ierr==10) {
      write_log(LOG_ERROR, "To many errors when sending binned data events (id=%d,type=%c), giving up", data.id, outId);
      break;
    }
    p++;
  } // i
    
  // send the last data packet
  if ((packetPointer > 4) && (ierr<10)) {
    
    message->data[3] = npackets;

    usleep(XRAY_DK_SLEEP_TIME_MS*1000);
    
    int iserr = 0;
    for (size_t k = 0; k < NUMBER_DK_REQ_TRIALS; k++) {
      iserr = 1;
      xcsp_buf_sendto(CSP_PRIO_NORM, CSP_DK_ADDRESS, CSP_DK_PORT, XRAY_PORT_DK_COM_ACK, CSP_O_NONE, buf, sizeof(dk_msg_store_ack_t)+packetPointer, 1000);
      if (waitForDkAck(DK_STORAGE_ACK_TIMEOUT) == 1) {
        iserr = 0;
        break;
      }
    }
    if(iserr)
      ierr++;

    usleep(XRAY_DK_SLEEP_TIME_MS*1000);
  }

  lastChunkId = getNextChunkId(STORAGE_DATA_ID) - 1;
  
  write_log(LOG_INFO_L2, "Binned data sent (id=%u,type=%c), firstChunk=%u, lastChunk=%u", data.id, outId, firstChunkId, lastChunkId);
  
  return ierr;
}

int xray_io_send_metainfo(const experiment_metainfo_t & info, uint32_t & firstChunkId, uint32_t & lastChunkId)
{
  firstChunkId = getNextChunkId(STORAGE_DATA_ID);

  uint8_t buf[sizeof(dk_msg_store_ack_t)+sizeof(experiment_metainfo_t)];
  dk_msg_store_ack_t * message = (dk_msg_store_ack_t *)buf;

  message->parent.cmd = DKC_STORE_ACK;
  message->port = STORAGE_METADATA_ID;
  message->host = CSP_DK_MY_ADDRESS;

  int ierr = 0;
  
  memcpy(message->data, &info, sizeof(experiment_metainfo_t));

  experiment_metainfo_t * info_packet = (experiment_metainfo_t*) message->data;

  // update packet
  info_packet->packetType = 'A';
  info_packet->firstChunkId = g_session_firstChunkId;
  info_packet->lastChunkId = g_session_lastChunkId;

  usleep(XRAY_DK_SLEEP_TIME_MS*1000);

  int iserr = 0;
  for (size_t k = 0; k < NUMBER_DK_REQ_TRIALS; k++) {
    iserr = 1;
    xcsp_buf_sendto(CSP_PRIO_NORM, CSP_DK_ADDRESS, CSP_DK_PORT, XRAY_PORT_DK_COM_ACK, CSP_O_NONE, buf, sizeof(buf), 1000);
    if (waitForDkAck(DK_STORAGE_ACK_TIMEOUT) == 1) {
      iserr = 0;
      break;
    }
  }
  if(iserr)
    ierr++;

  usleep(XRAY_DK_SLEEP_TIME_MS*1000);

  lastChunkId = getNextChunkId(STORAGE_DATA_ID) - 1;

  write_log(LOG_INFO_L2, "Experiment metainfo sent (start_nb=%u,exp_nb=%u), firstChunk=%u, lastChunk=%u", info.start_nb, info.exp_nb, firstChunkId, lastChunkId);

  return ierr;
}

int xray_io_send_expconfig(const experiment_config_t & config)
{
  uint8_t buf[sizeof(dk_msg_store_ack_t)+sizeof(experiment_config_t)];
  dk_msg_store_ack_t * message = (dk_msg_store_ack_t *)buf;

  message->parent.cmd = DKC_STORE_ACK;
  message->port = STORAGE_METADATA_ID | 0x80;
  message->host = CSP_DK_MY_ADDRESS;

  memcpy(message->data, &config, sizeof(experiment_config_t));
  
  int ierr = 0;
  
  usleep(XRAY_DK_SLEEP_TIME_MS*1000);

  int iserr = 0;
  for (size_t k = 0; k < NUMBER_DK_REQ_TRIALS; k++) {
    iserr = 1;
    xcsp_buf_sendto(CSP_PRIO_NORM, CSP_DK_ADDRESS, CSP_DK_PORT, XRAY_PORT_DK_COM_ACK, CSP_O_NONE, buf, sizeof(buf), 1000);
    if (waitForDkAck(DK_STORAGE_ACK_TIMEOUT) == 1) {
      iserr = 0;
      break;
    }
  }
  if(iserr)
    ierr++;

  usleep(XRAY_DK_SLEEP_TIME_MS*1000);

  write_log(LOG_INFO_L2, "Experiment config sent to DK");

  return ierr;
}

int xray_io_send_status(const xray_status_t & status)
{
  uint8_t buf[sizeof(dk_msg_store_ack_t)+sizeof(xray_status_t)];
  dk_msg_store_ack_t * message = (dk_msg_store_ack_t *)buf;

  message->parent.cmd = DKC_STORE_ACK;
  message->port = STORAGE_HK_ID;
  message->host = CSP_DK_MY_ADDRESS;

  memcpy(message->data, &status, sizeof(xray_status_t));

  int ierr = 0;

  usleep(XRAY_DK_SLEEP_TIME_MS*1000);

  int iserr = 0;
  for (size_t k = 0; k < NUMBER_DK_REQ_TRIALS; k++) {
    iserr = 1;
    xcsp_buf_sendto(CSP_PRIO_NORM, CSP_DK_ADDRESS, CSP_DK_PORT, XRAY_PORT_DK_COM_ACK, CSP_O_NONE, buf, sizeof(buf), 1000);
    if (waitForDkAck(DK_STORAGE_ACK_TIMEOUT) == 1) {
      iserr = 0;
      break;
    }
  }
  if(iserr)
    ierr++;

  usleep(XRAY_DK_SLEEP_TIME_MS*1000);

  write_log(LOG_INFO_L2, "Status sent to DK");

  return ierr;
}
