/*
 * dkHandler.c
 *
 *  Author: klaxalk, zdemat
 */ 

#include "csp/csp.h"
#include "xray.h"
#include "dkHandler.h"
#include "mainTask.h"
#include "cspTask.h"
#include "csp/csp_endian.h"
#include <unistd.h>
#include "Log.h"

uint8_t createStorage(uint8_t id, uint16_t size) {

  uint8_t buf[sizeof(dk_msg_create_t)];
  dk_msg_create_t * message = (dk_msg_create_t *)buf;
	
  message->parent.cmd = DKC_CREATE;
  message->port = id;
  message->conf_siz = csp_hton16(size);

  usleep(XRAY_DK_SLEEP_TIME_MS*1000);
  
  for (size_t k = 0; k < NUMBER_DK_REQ_TRIALS; k++) {

    xcsp_buf_sendto(CSP_PRIO_NORM, CSP_DK_ADDRESS, CSP_DK_PORT, XRAY_PORT_DK_COM_ACK, CSP_O_NONE, buf, sizeof(dk_msg_create_t), 1000);

    if (waitForDkAck() == 1) {
      usleep(XRAY_DK_SLEEP_TIME_MS*1000);
      return 1;
    }
  }
	
  return 0;
};

uint8_t wipeStorage(uint8_t id) {

  uint8_t buf[sizeof(dk_msg_storage_t)];
  dk_msg_storage_t * message = (dk_msg_storage_t *)buf;
  
  message->parent.cmd = DKC_WIPE;
  message->port = id;
  message->host = CSP_DK_MY_ADDRESS;
	
  usleep(XRAY_DK_SLEEP_TIME_MS*1000);
  
  for (size_t k = 0; k < NUMBER_DK_REQ_TRIALS; k++) {
    
    xcsp_buf_sendto(CSP_PRIO_NORM, CSP_DK_ADDRESS, CSP_DK_PORT, XRAY_PORT_DK_COM_ACK, CSP_O_NONE, buf, sizeof(dk_msg_storage_t), 1000);
    
    if (waitForDkAck() == 1) {
      usleep(XRAY_DK_SLEEP_TIME_MS*1000);
      return 1;
    }
  }
	
  return 0;
}

uint8_t clearStorage(uint8_t id) {
	
  if (wipeStorage(id) == 1) {
    /* WIPE keeps the config, no need for update
    uint16_t conf_sz = (id==STORAGE_METADATA_ID) ? sizeof(experiment_config_t) : DEFAULT_CONF_CHUNK_SIZE;
    if (createStorage(id, conf_sz) == 1) // to update config (???)
      return 1;
    else
      return 0;
    */
    return 1;
  } else  
    return 0;
}

uint8_t createStorages() {	

  // clear all storages
  
  uint8_t i;
  uint8_t out = 1;
  
  for (i = 1; i <= NUMBER_OF_STORAGES; i++) {		
    clearStorage(i);
    uint16_t conf_sz = (i==STORAGE_METADATA_ID) ? sizeof(experiment_config_t) : DEFAULT_CONF_CHUNK_SIZE; 
    out *= createStorage(i, conf_sz);
  }

  save_start_nb_dk((uint16_t)get_global_app_settings().start_nb);
    
  return out;
}

int save_start_nb_dk(uint16_t start_nb)
{
  const size_t conf_sz = DEFAULT_CONF_CHUNK_SIZE;
  
  uint8_t buffer[sizeof(dk_msg_store_ack_t)+conf_sz];

  dk_msg_store_ack_t *req = (dk_msg_store_ack_t *)buffer;

  req->parent.cmd = DKC_STORE_ACK;
  req->port = STORAGE_HK_ID | 0x80;
  req->host = CSP_DK_MY_ADDRESS;

  int ierr = 0;
  
  //set your conf scratchpad here
  memcpy(req->data, &start_nb, 2);
  memset(req->data+2, 0, conf_sz-2);

  usleep(XRAY_DK_SLEEP_TIME_MS*1000);

  int iserr = 0;
  /*for (size_t k = 0; k < NUMBER_DK_REQ_TRIALS; k++) {
    iserr = 1;
    xcsp_buf_sendto(CSP_PRIO_NORM, CSP_DK_ADDRESS, CSP_DK_PORT, XRAY_PORT_DK_COM_ACK, CSP_O_NONE, buffer, sizeof(dk_msg_store_ack_t)+conf_sz, 1000);
    if (waitForDkAck() == 1) {
      iserr = 0;
      break;
    }
  }
  if(iserr)
  ierr++; */

  // TODO: reply size is 8 instead of 6
  uint8_t reply_buf[sizeof(dk_reply_common_t)+10];
  dk_reply_common_t * reply = (dk_reply_common_t*) reply_buf;

  for (size_t k = 0; k < NUMBER_DK_REQ_TRIALS; k++) {
    iserr = 1;
    if (!csp_transaction(CSP_PRIO_NORM, CSP_DK_ADDRESS, CSP_DK_PORT, 1000, buffer, sizeof(dk_msg_store_ack_t)+conf_sz,
  		         reply_buf, 0)) {
      write_log(LOG_ERROR, "dk store conf error!");
      usleep(XRAY_DK_SLEEP_TIME_MS*1000);
    } else {
      iserr = 0;
      break;
    }
  }

  if(iserr)
    return -1;

  // this communication is a complete mess
  /*reply->err_no = csp_ntoh32(reply->err_no);

  if (reply->err_no != 0) {
    write_log(LOG_ERROR, "dk error %d!", reply->err_no);
    return -1;
    }*/

  usleep(XRAY_DK_SLEEP_TIME_MS*1000);

  return ierr;
}

uint16_t get_start_nb_dk()
{
  const size_t conf_sz = DEFAULT_CONF_CHUNK_SIZE;
  
  uint8_t buffer[sizeof(dk_msg_get_t)];

  dk_msg_get_t *req = (dk_msg_get_t *)buffer;

  req->parent.cmd = DKC_GET_RAW;
  req->port = STORAGE_HK_ID;
  req->host = CSP_DK_MY_ADDRESS;
  req->chunk = 0; // config

  int ierr = 0;

  usleep(XRAY_DK_SLEEP_TIME_MS*1000);

  uint8_t reply_buf[DEFAULT_CONF_CHUNK_SIZE];

  int iserr = 0;
  for (size_t k = 0; k < NUMBER_DK_REQ_TRIALS; k++) {
    iserr = 1;
    if (!csp_transaction(CSP_PRIO_NORM, CSP_DK_ADDRESS, CSP_DK_PORT, 1000, buffer, sizeof(dk_msg_get_t),
  		         reply_buf, conf_sz)) {
      write_log(LOG_ERROR, "dk get raw error!");
      usleep(XRAY_DK_SLEEP_TIME_MS*1000);
    } else {
      iserr = 0;
      break;
    }
  }

  uint16_t start_nb = 0;

  if(iserr)
    return start_nb;

  memcpy(&start_nb, reply_buf, 2);

  usleep(XRAY_DK_SLEEP_TIME_MS*1000);

  return start_nb;
}

int get_experiment_config_dk(experiment_config_t & cfg)
{
  const size_t conf_sz = sizeof(experiment_config_t);

  uint8_t buffer[sizeof(dk_msg_get_t)];

  dk_msg_get_t *req = (dk_msg_get_t *)buffer;

  req->parent.cmd = DKC_GET_RAW;
  req->port = STORAGE_METADATA_ID;
  req->host = CSP_DK_MY_ADDRESS;
  req->chunk = 0; // config
  
  int ierr = 0;

  usleep(XRAY_DK_SLEEP_TIME_MS*1000);

  uint8_t reply_buf[sizeof(experiment_config_t)];

  int iserr = 0;
  for (size_t k = 0; k < NUMBER_DK_REQ_TRIALS; k++) {
    iserr = 1;
    if (!csp_transaction(CSP_PRIO_NORM, CSP_DK_ADDRESS, CSP_DK_PORT, 1000, buffer, sizeof(dk_msg_get_t),
                         reply_buf, conf_sz)) {
      write_log(LOG_ERROR, "dk get raw error!");
      usleep(XRAY_DK_SLEEP_TIME_MS*1000);
    } else {
      iserr = 0;
      break;
    }
  }

  if(iserr)
    return 1;

  memcpy(&cfg, reply_buf, conf_sz);

  usleep(XRAY_DK_SLEEP_TIME_MS*1000);

  return 0;
}

#ifdef XXXX

uint32_t waitForTimeAck() {
	
	int32_t time;
	
	if (pdTRUE == xQueueReceive(xCSPTimeQueue, &time, 1000)) {
		
		return (uint32_t) time;
	}

	return 0;
}

uint32_t getTime() {
	
	csp_cmp_msg_t * msg = (csp_cmp_msg_t *) outcomingPacket->data;
	msg->type = 0; //CSP_CMP_REQUEST;
	msg->code = 6; //CSP_CMP_CLOCK;
	msg->tv_sec = 0; //MUST be set to 0; clock>0 will modify OBC time!
	msg->tv_nsec = 0;
		
	uint32_t time;
	
	uint8_t k;
	for (k = 0; k < 3; k++) {
				
		outcomingPacket->length = sizeof(csp_cmp_msg_t);
		csp_sendto(CSP_PRIO_NORM, CSP_OBC_ADDRESS, CSP_CMP, 20, CSP_O_NONE, outcomingPacket, 1000);
				
		if (pdTRUE == xQueueReceive(xCSPTimeQueue, &time, 100)) {
			
			return csp_ntoh32(time);
		}
	}
	
	return 0;
}

uint8_t getAttitude(int16_t * attitude, int16_t * position) {
	
	timestamp_t * req_time = (timestamp_t *) &outcomingPacket->data;
	
	req_time->tv_sec = csp_hton32(imageParameters.time);
	req_time->tv_nsec = 0;
	
	adcs_att_t attitudeIn;
	uint8_t i;
		
	uint8_t k;
	for (k = 0; k < 3; k++) {
		
		outcomingPacket->length = sizeof(timestamp_t);
		csp_sendto(CSP_PRIO_NORM, CSP_OBC_ADDRESS, OBC_PORT_ADCS, 19, CSP_O_NONE, outcomingPacket, 1000);
		
		if (pdTRUE == xQueueReceive(xCSPAttitudeQueue, &attitudeIn, 300)) {
			
			for (i = 0; i < 7; i++) {
				
				attitude[i] = csp_ntoh16(attitudeIn.attitude[i]);
			}
			
			for (i = 0; i < 3; i++) {
				
				position[i] = csp_ntoh16(attitudeIn.position[i]);
			}
			
			return 1;
		}
	}
	
	for (i = 0; i < 7; i++) {
		
		attitude[i] = 0;
	}
	
	for (i = 0; i < 3; i++) {
		
		position[i] = 0;
	}
	
	return 0;
}

#endif
