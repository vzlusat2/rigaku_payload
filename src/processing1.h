/*
 * processing.h
 *
 *  Author: Zdenek Matej
 */

#include "processing.h"

#ifndef _XRAY_PROCESSING1_H_
#define _XRAY_PROCESSING1_H_

struct filtering_workspace_cluster1_t {
  FRAME_DIM_TYPE  * cluster_rows;
  FRAME_DIM_TYPE  * cluster_cols;
  FRAME_DATA_TYPE * cluster_data;
  bool            * processed;
  int cluster_sz;
  FRAME_DIM_TYPE cluster_row_low;
  FRAME_DIM_TYPE cluster_row_hgh;
  FRAME_DIM_TYPE cluster_col_low;
  FRAME_DIM_TYPE cluster_col_hgh;
  int frame_sz;
  int height;
  int width;
};

int new_filtering_workspace_cluster1(filtering_workspace_cluster1_t & workspace);
int delete_filtering_workspace_cluster1(filtering_workspace_cluster1_t & workspace);
int filter_cluster_events1(FRAME_DATA_TYPE * filtered, const FRAME_DATA_TYPE * frame,
                           int cluster_dim,
                           bool use_calib,
                           int det_mode,
                           size_t height, size_t width,
                           filtering_workspace_cluster1_t & wsp);

#endif /* _XRAY_PROCESSING1_H_ */
