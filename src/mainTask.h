/*
 * mainTask.h
 *
 *  Author: Zdenek Matej
 */ 

#include "detectorTask.h"
#include "cspTask.h"

#ifndef _MAINTASK_H_
#define _MAINTASK_H_

struct experiment_settings_t
{
  experiment_settings_t():exp_nb(0), outputform(0), filtering(1), en_hist_limit(0), power_off(0), scan_mode(0), count_roi(0), count_threshold(0) {};
  global_settings_t application;
  detector_settings_t detector;
  uint32_t exp_nb;
  uint16_t outputform;
  uint8_t filtering;
  uint16_t en_hist_limit;
  uint8_t power_off;
  uint8_t scan_mode;
  uint8_t count_roi;
  uint32_t count_threshold;
};

int mainTask(const void *, void *);
uint8_t waitForDkAck(const long rel_time_ms = 3000);
int xray_system_shutdown();
int load_experiment_config_dk(bool allow_exec);

#endif /* _MAINTASK_H_ */
