/*
 * detectorTask.h
 *
 *  Author: Zdenek Matej
 */ 

#ifndef _DETECTOR_TASK_H_
#define _DETECTOR_TASK_H_

#include "xray.h"

struct roi_settings_t
{
  roi_settings_t(): row(0), col(0), height(0), width(0), method(0) {};
  roi_settings_t(const roi_settings_t & s): row(s.row), col(s.col), height(s.height), width(s.width), method(s.method) {};
  bool is_empty() const { return (width*height)==0; };
  void set_empty() { width = 0; height = 0; } 
  unsigned short row;
  unsigned short col;
  unsigned short height;
  unsigned short width;
  unsigned short method;
};

struct detector_settings_t
{
  detector_settings_t():frameTime(0.1), framePeriod(0.0), nFrames(1), mode(0), bias(XRAY_DEFAULT_BIAS), threshold(5.0)
  {
    rois[0].row = 0;            rois[0].col = 0;           rois[0].height= DET_HEIGHT;   rois[0].width = DET_WIDTH;
    rois[1].row = 0;            rois[1].col = 0;           rois[1].height= DET_HEIGHT/2; rois[1].width = DET_WIDTH/2;
    rois[2].row = 0;            rois[2].col = DET_WIDTH/2; rois[2].height= DET_HEIGHT/2; rois[2].width = DET_WIDTH/2;
    rois[3].row = DET_HEIGHT/2; rois[3].col = 0;           rois[3].height= DET_HEIGHT/2; rois[3].width = DET_WIDTH/2;
    rois[4].row = DET_HEIGHT/2; rois[4].col = DET_WIDTH/2; rois[4].height= DET_HEIGHT/2; rois[4].width = DET_WIDTH/2;
  };
  detector_settings_t(const detector_settings_t& s):frameTime(s.frameTime), framePeriod(s.framePeriod), nFrames(s.nFrames), mode(s.mode), bias(s.bias), threshold(s.threshold)
  {
    for(size_t i=0; i<DET_NROIS; i++)
      rois[i] = s.rois[i];
  };
  double frameTime;
  double framePeriod;
  int nFrames;
  int mode;
  double bias;
  double threshold;
  roi_settings_t rois[DET_NROIS];
};
  
typedef enum {
 DETECTOR_TASK_VOID = TASK_VOID,
 DETECTOR_INIT,
 DETECTOR_SET_ACQ_MODE,
 DETECTOR_SET_BIAS,
 DETECTOR_SET_THRESHOLD,
 DETECTOR_MEASURE,
 DETECTOR_ABORT,
 DETECTOR_SYSTEM_SHUTDOWN,
 DETECTOR_UPDATE,
 DETECTOR_SLEEP,
} detectorEventType_t;

typedef struct DETECTOR_TASK_EVT_DTA
{
  detectorEventType_t eventType; /* Tells the receiving task what the event is. */
  void *pvData; /* Holds or points to any data associated with the event. */
} detectorEvent_t;

int detectorTask(const void *, void *);

extern float *g_det_caliba;
extern float *g_det_calibb;
extern float *g_det_calibc;
extern float *g_det_calibt;
extern bool  g_det_calib_avail;

int new_global_detector_calibration();
int delete_global_detector_calibration();
int load_detector_calibration(const char * detector_name);

#endif /* _DETECTOR_TASK_H_ */
