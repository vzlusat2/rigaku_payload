
#ifndef _RITE_LOG_H_
#define _RITE_LOG_H_

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
  LOG_INFO = 0,
  LOG_WARNING,
  LOG_ERROR,
  LOG_PACKET,
  LOG_INFO_L2,
} log_type_t;

int write_log(log_type_t event, const char *format, ...);
 
#ifdef __cplusplus
}
#endif
  
#endif /* _RITE_LOG_H_ */
