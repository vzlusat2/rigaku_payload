#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include "Log.h"
  
int write_log(log_type_t event, const char *format, ...)
{
  // print time stamp
  char tstr[64];
  struct timeval tv;
  struct tm * timeinfo;
  
  gettimeofday(&tv, NULL);
  timeinfo = localtime (&tv.tv_sec);

  strftime(tstr, sizeof(tstr), "%F %T", timeinfo);

  // print type string
  switch(event) {
  case (LOG_INFO):
    printf("[%s.%03ld] ", tstr, tv.tv_usec/1000);
    printf("[info] ");
    break;
  case (LOG_WARNING):
    printf("[%s.%03ld] ", tstr, tv.tv_usec/1000);
    printf("[warning] ");
    break;
  case (LOG_ERROR):
    printf("[%s.%03ld] ", tstr, tv.tv_usec/1000);
    printf("[error] ");
    break;
  case (LOG_PACKET):
    #ifdef DEBUG
    printf("[%s.%03ld] ", tstr, tv.tv_usec/1000);
    printf("[packet] ");
    #endif
    break;
  case (LOG_INFO_L2):
    #ifdef DEBUG
    printf("[%s.%03ld] ", tstr, tv.tv_usec/1000);
    printf("[info2] ");
    #endif
    break;
  default:
    break;
  }

  // print variable argument list
  va_list args;
  va_start(args, format);

  switch(event) {
  case (LOG_INFO):
    vprintf(format, args);
    printf("\n");
    fflush(stdout);
    break;
  case (LOG_WARNING):
    vprintf(format, args);
    printf("\n");
    fflush(stdout);
    break;
  case (LOG_ERROR):
    vprintf(format, args);
    printf("\n");
    fflush(stdout);
    break;
  case (LOG_PACKET):
    #ifdef DEBUG
    vprintf(format, args);
    printf("\n");
    fflush(stdout);
    #endif
    break;
  case (LOG_INFO_L2):
    #ifdef DEBUG
    vprintf(format, args);
    printf("\n");
    fflush(stdout);
    #endif
    break;
  default:
    break;
  }
  
  va_end(args);
}
