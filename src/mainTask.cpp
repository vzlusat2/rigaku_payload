
/* -------------------------------------------------------------------- */
/*	The main task														*/
/* -------------------------------------------------------------------- */

#include <csp/csp.h>

#include <unistd.h>

#include "pxcapi.h"

#include "xray.h"
#include "Log.h"
#include "mainTask.h"
#include "cspTask.h"
#include "errorCodes.h"
#include "dkHandler.h"
#include "ioTask.h"

#include <csp/csp_cmp.h>
#include <csp/csp_endian.h>

#include <string>

using namespace std;

// do not export out of Task (global_settings initialized by cspTask)
experiment_settings_t g_ex_settings;

int xcsp_reply_packet(uint8_t        prio,
                      csp_packet_t * packet,
                      uint32_t       opts,
                      void         * replybuf,
                      int            replylen,
                      uint32_t       timeout)
{
  uint8_t dest = packet->id.src;
  uint8_t dport = packet->id.sport;
  uint8_t sport = packet->id.dport; 

  csp_buffer_free(packet);

  return xcsp_buf_sendto(prio, dest, dport, sport, opts, replybuf, replylen, timeout);
}

void replyMsgBin(csp_packet_t *packet, csp_conn_t * conn, const uint8_t * msg, size_t len) {
  
  csp_buffer_free(packet);
  
  csp_packet_t * reply_packet;
  
  reply_packet = (csp_packet_t *)csp_buffer_get(len);
  if (!reply_packet) {
    csp_close(conn);
    return;
  }
	
  // Copy message to packet
  memcpy(reply_packet->data, msg, len);
  reply_packet->length = len;

  if (!csp_send(conn, reply_packet, 0))
    csp_buffer_free(reply_packet);

  csp_close(conn);
}

void replyMsg(csp_packet_t *packet, csp_conn_t * conn, const char * msg) {

  csp_buffer_free(packet);

  size_t len = strlen(msg)+1;

  csp_packet_t * reply_packet;

  reply_packet = (csp_packet_t *)csp_buffer_get(len);
  if (!reply_packet) {
    csp_close(conn);
    return;
  }

  // Copy message to packet                                                                                                                                                                                                                   
  strcpy((char*)reply_packet->data, msg);
  reply_packet->length = len;

  if (!csp_send(conn, reply_packet, 0))
    csp_buffer_free(reply_packet);

  csp_close(conn);
}

void replyPacketOk(csp_packet_t *packet)
{
  char msg[XRAY_REPLY_BUF_SZ] = "OK";
  xcsp_reply_packet(CSP_PRIO_NORM, packet, CSP_O_NONE, msg, strlen(msg)+1, 1000);
}

void replyPacketErr(csp_packet_t *packet, uint8_t error) {
  char msg[XRAY_REPLY_BUF_SZ];
  sprintf(msg, "E%u", error);
  xcsp_reply_packet(CSP_PRIO_NORM, packet, CSP_O_NONE, msg, strlen(msg)+1, 1000);
}

void replyPacketStatus(csp_packet_t *packet, xray_status_t status) {
  xcsp_reply_packet(CSP_PRIO_NORM, packet, CSP_O_NONE, &status, sizeof(xray_status_t), 1000);
}

void replyOk(csp_packet_t *packet, csp_conn_t * conn)
{
  char msg[XRAY_REPLY_BUF_SZ] = "OK";
  replyMsg(packet, conn, msg);
}

void replyErr(csp_packet_t *packet, csp_conn_t * conn, uint8_t error) {
  char msg[XRAY_REPLY_BUF_SZ];
  sprintf(msg, "E%u", error);
  replyMsg(packet, conn, msg);
}

void replyStatus(csp_packet_t *packet, csp_conn_t * conn, xray_status_t status) {
  replyMsgBin(packet, conn, (uint8_t*)(&status), sizeof(xray_status_t));
}

uint8_t waitForDkAck(const long rel_time_ms) {
  
  int32_t err_no = 1;
	
  if (!sCSPAckConsumable.consume(err_no, rel_time_ms * 1ms)) { // no consume err

    //write_log(LOG_INFO, "DK ACK received, err_no=%d", err_no);
    
    if (err_no != 0)	
      return 0;
    else
      return 1;
  }

  write_log(LOG_ERROR, "DK ACK timeout (%d ms)", rel_time_ms);

  return 0;
}

int xray_system_shutdown()
{ 
  int rc = pxcExit();
  if (rc) {
    write_log(LOG_ERROR, "Error when deinitializing Pixet, error: %d\n", rc);
  }
  
  sleep(1);

  #ifdef PRODUCTION
  int ret = system("halt -d 1 &");
  #else
  int ret = system("reboot -d 1 &");
  #endif
  if (ret) {
    write_log(LOG_ERROR, "Error system shutdown\n");
    return ERROR_SYSTEM_SHUTDOWN;
  }

  write_log(LOG_INFO, "System shutdown requested!\n");
  return 0;
}

int xray_detector_set_exposure(int msec)
{
  g_ex_settings.detector.frameTime = msec/1000.;
  g_ex_settings.detector.framePeriod = 0.; // explicit reset
  
  write_log(LOG_INFO, "Detector frameTime=%g (s)", g_ex_settings.detector.frameTime);

  return 0;
}

int xray_detector_sleep(int msec)
{
  detectorEvent_t * detEvt = new detectorEvent_t; // deleted by handler

  detEvt->eventType = DETECTOR_SLEEP;
  detEvt->pvData = new uint32_t(msec); // deleted by handler

  sDetectorEventQueue.PostMsg(detEvt);
  
  return 0;
}

int xray_detector_set_period(int msec)
{
  g_ex_settings.detector.framePeriod = msec/1000.;

  write_log(LOG_INFO, "Detector framePeriod=%g (s)", g_ex_settings.detector.framePeriod);

  return 0;
}

int xray_detector_set_nframes(int nframes)
{
  g_ex_settings.detector.nFrames = nframes;

  write_log(LOG_INFO, "Detector nFrames=%d", g_ex_settings.detector.nFrames);

  return 0;
}

int xray_detector_set_roi(int id, int row, int col, int height, int width)
{
  if((id<DET_NROIS) && (row>=0) && (col>=0) && (row<=DET_HEIGHT) && (col<=DET_WIDTH) && ((row+height)<=DET_HEIGHT) && ((col+width)<=DET_WIDTH)) {
    g_ex_settings.detector.rois[id].row    = row;
    g_ex_settings.detector.rois[id].col	   = col;
    g_ex_settings.detector.rois[id].height = height;
    g_ex_settings.detector.rois[id].width  = width;
    write_log(LOG_INFO, "Detector set ROI id=%d [%d, %d, %d, %d]", id, row, col, height, width);
    return 0;
  } else {
    write_log(LOG_ERROR, "Error setting ROI id=%d [%d, %d, %d, %d]", id, row, col, height, width);
    return ERROR_DETECTOR_SETROI;
  }
}

int xray_set_outputform(int outputform)
{
  g_ex_settings.outputform = outputform;

  write_log(LOG_INFO, "Experiment outputform=%d", g_ex_settings.outputform);

  return 0;
}

int xray_set_filtering(int filtering)
{
  g_ex_settings.filtering = filtering;

  write_log(LOG_INFO, "Experiment filtering=%d", g_ex_settings.filtering);

  return 0;
}

int xray_set_energy_histogram_limit(int limit)
{
  g_ex_settings.en_hist_limit = limit;

  write_log(LOG_INFO, "Experiment en_hist_limit=%d", g_ex_settings.en_hist_limit);

  return 0;
}

int xray_set_count_treshold(uint32_t threshold, uint8_t id)
{
  g_ex_settings.count_threshold = threshold;
  g_ex_settings.count_roi = id % DET_NROIS;

  write_log(LOG_INFO, "Experiment count_threshold=%u, count_roi=%u", g_ex_settings.count_threshold, g_ex_settings.count_roi);

  return 0;
}

void xray_measure(uint8_t mode, bool shutdown)
{
  // measure
  detectorEvent_t * detEvt = new detectorEvent_t; // deleted by handler                                                                                                                                                               
  experiment_settings_t * expConfig = new experiment_settings_t(g_ex_settings); // deleted by handler                                                                                                                                 
  detEvt->eventType = DETECTOR_MEASURE;
  expConfig->scan_mode = mode; // set scan mode                                                                                                                                                                                       
  detEvt->pvData = expConfig;
  g_ex_settings.exp_nb++; // increment experiment number (for the next one)                                                                                                                                                           
  sDetectorEventQueue.PostMsg(detEvt);

  // shutdown
  if(shutdown) {
    detectorEvent_t * detEvtSD = new detectorEvent_t; // deleted by handler                                                                                                                                                               
    detEvtSD->eventType = DETECTOR_SYSTEM_SHUTDOWN;
    detEvtSD->pvData = NULL;
    //int ierr, rv = -1; // rv deleted here                                                                                                                                                                                             
    //int rv = xray_system_shutdown();                                                                                                                                                                                                  
    sDetectorEventQueue.PostMsg(detEvtSD);
  }
}

int xray_set_parameters(const xray_cmd_setparam_t * pck)
{
  g_ex_settings.detector.mode        = pck->mode;
  g_ex_settings.detector.threshold   = pck->threshold;
  g_ex_settings.detector.frameTime   = pck->frameTime/1000.;
  g_ex_settings.detector.framePeriod = 0.; // explicit reset
  g_ex_settings.detector.nFrames     = pck->nFrames;
  g_ex_settings.filtering            = pck->filtering;
  g_ex_settings.outputform           = pck->outputform;
  g_ex_settings.scan_mode            = pck->scanMode;
  g_ex_settings.count_threshold      = pck->countThreshold;
  g_ex_settings.count_roi            = pck->countROI;
  g_ex_settings.en_hist_limit        = pck->en_hist_limit;
    
  detectorEvent_t * detEvt = new detectorEvent_t; // deleted by handler                                                                                                                                                               
  experiment_settings_t * expConfig = new experiment_settings_t(g_ex_settings); // deleted by handler

  detEvt->eventType = DETECTOR_UPDATE;                                                                                                                                                                                       
  detEvt->pvData = expConfig;

  sDetectorEventQueue.PostMsg(detEvt);

  return 0;
}

int xray_save_config(bool valid)
{
  ioEvent_t * ioEvt = new ioEvent_t; // deleted by handler
  experiment_config_t * ec = new experiment_config_t; // deleted by handler

  ioEvt->eventType = XRAY_IO_SEND_CONFIG;
  ioEvt->pvData = ec;
  
  ec->packetType = 'C';
  ec->valid = valid;
  ec->automeasure = 0;
  
  // copy setting drom g_ex_settings
  ec->turnoff        = g_ex_settings.power_off;
  ec->mode           = g_ex_settings.detector.mode;
  ec->filtering      = g_ex_settings.filtering;
  ec->scanMode       = g_ex_settings.scan_mode;
  ec->countROI       = g_ex_settings.count_roi;
  ec->threshold      = g_ex_settings.detector.threshold;
  ec->bias           = g_ex_settings.detector.bias;
  ec->frameTime      = g_ex_settings.detector.frameTime;
  ec->framePeriod    = g_ex_settings.detector.framePeriod;
  ec->nFrames        = g_ex_settings.detector.nFrames;
  ec->outputform     = g_ex_settings.outputform;
  ec->countThreshold = g_ex_settings.count_threshold;
  ec->en_hist_limit  = g_ex_settings.en_hist_limit;
  
  for(size_t iroi=0; iroi<DET_NROIS; iroi++) {
      ec->rois[iroi][0] = g_ex_settings.detector.rois[iroi].row;
      ec->rois[iroi][1] = g_ex_settings.detector.rois[iroi].col;
      ec->rois[iroi][2] = g_ex_settings.detector.rois[iroi].height;
      ec->rois[iroi][3] = g_ex_settings.detector.rois[iroi].width;
  }

  int ierr, rv = -1; // rv deleted here
  ierr = sIOEventQueue.PostMsg(ioEvt, 3000, &rv, sizeof(rv));
  
  return rv;
}

int xray_set_autoexec(int autoexec, int turnoff)
{
  experiment_config_t * cfg = new experiment_config_t; // (if no err) deleted by handler
  int rv; // deleted here

  // get config
  rv = get_experiment_config_dk(*cfg);

  if(rv) {
    delete cfg;
    return rv;
  }

  cfg->automeasure = (uint8_t)autoexec;
  cfg->turnoff = turnoff;

  // save config
  ioEvent_t * ioEvt = new ioEvent_t; // deleted by handler
  
  ioEvt->eventType = XRAY_IO_SEND_CONFIG;
  ioEvt->pvData = cfg;

  int ierr;
  rv = -1;
  ierr = sIOEventQueue.PostMsg(ioEvt, 3000, &rv, sizeof(rv));
  
  return rv;
}

int xray_send_status(const xray_status_t & status)
{
  xray_status_t * msg = new xray_status_t; // (if no err) deleted by handler
  int rv; // deleted here
    
  // save config
  ioEvent_t * ioEvt = new ioEvent_t; // deleted by handler                                                                                                                                                                                    
  ioEvt->eventType = XRAY_IO_SEND_STATUS;
  ioEvt->pvData = msg;

  memcpy(msg, &status, sizeof(xray_status_t));
  
  int ierr;
  rv = -1;
  ierr = sIOEventQueue.PostMsg(ioEvt, 3000, &rv, sizeof(rv));

  return rv;
}

int load_experiment_config_dk(bool allow_exec)
{
  experiment_config_t cfg;
  int rv = get_experiment_config_dk(cfg);
  if(rv)
    return rv;

  // check if config is valid
  if((cfg.packetType!='C') || !cfg.valid)
    return 1;

  // copy config into g_ex_settings
  uint8_t auto_exec = cfg.automeasure;
  
  // copy setting drom g_ex_settings
  g_ex_settings.power_off            = cfg.turnoff;
  g_ex_settings.detector.mode        = cfg.mode;
  g_ex_settings.filtering            = cfg.filtering;
  g_ex_settings.scan_mode            = cfg.scanMode;
  g_ex_settings.count_roi            = cfg.countROI;
  g_ex_settings.detector.threshold   = cfg.threshold;
  g_ex_settings.detector.bias        = cfg.bias;
  g_ex_settings.detector.frameTime   = cfg.frameTime;
  g_ex_settings.detector.framePeriod = cfg.framePeriod;
  g_ex_settings.detector.nFrames     = cfg.nFrames;
  g_ex_settings.outputform           = cfg.outputform;
  g_ex_settings.count_threshold      = cfg.countThreshold;
  g_ex_settings.en_hist_limit        = cfg.en_hist_limit;
  
  for(size_t iroi=0; iroi<DET_NROIS; iroi++) {
    g_ex_settings.detector.rois[iroi].row    = cfg.rois[iroi][0];
    g_ex_settings.detector.rois[iroi].col    = cfg.rois[iroi][1];
    g_ex_settings.detector.rois[iroi].height = cfg.rois[iroi][2];
    g_ex_settings.detector.rois[iroi].width  = cfg.rois[iroi][3];
  }

  write_log(LOG_INFO, "Loaded experimental configuration from DK");
  
  // send cmds directly to detector
  {
    detectorEvent_t * detEvt = new detectorEvent_t; // deleted by handler                                                                                                                                                               
    experiment_settings_t * expConfig = new experiment_settings_t(g_ex_settings); // deleted by handler
    detEvt->eventType = DETECTOR_UPDATE;                                                                                                                                                                                       
    detEvt->pvData = expConfig;
    sDetectorEventQueue.PostMsg(detEvt);
  }
  
  // (autoexec) measure
  if(allow_exec && auto_exec) {
    detectorEvent_t * detEvt = new detectorEvent_t; // deleted by handler
    experiment_settings_t * expConfig = new experiment_settings_t(g_ex_settings); // deleted by handler
    detEvt->eventType = DETECTOR_MEASURE;
    detEvt->pvData = expConfig;
    g_ex_settings.exp_nb++; // increment experiment number (for the next measurement)
    sDetectorEventQueue.PostMsg(detEvt);
  }
  
  // (autoexec) shutdown
  if(allow_exec && auto_exec && g_ex_settings.power_off) {
    detectorEvent_t * detEvtSD = new detectorEvent_t; // deleted by handler
    detEvtSD->eventType = DETECTOR_SYSTEM_SHUTDOWN;
    detEvtSD->pvData = NULL;
    sDetectorEventQueue.PostMsg(detEvtSD);
  }

  return 0;
}

template <class T_event, class T_event_type> int check_queue_status(WorkerThread & queue)
{
  int status = queue.Empty();

  if(status) {
    // submit a void task
    T_event * evt = new T_event; // deleted by handler                                                                                                                                                              
    evt->eventType = (T_event_type)TASK_VOID;
    evt->pvData = NULL;
    int ierr, rv = -1; // rv deleted here                                                                                                                                                                                              
    ierr = queue.PostMsg(evt, 200, &rv, sizeof(rv));	  
    status = (ierr==0) && (rv==0);
  }
  
  return status;
}

int xray_get_status(xray_status_t & status)
{
  status.packetType = 'S';
  status.status = 0;

  // time
  csp_timestamp_t ts;
  csp_clock_get_time(&ts);
  status.tv_sec = ts.tv_sec;
  status.tv_nsec = ts.tv_nsec;
  
  // status detector
  status.status |= (check_queue_status<detectorEvent_t,detectorEventType_t>(sDetectorEventQueue) << 0);

  // status io
  status.status |= (check_queue_status<ioEvent_t,ioEventType_t>(sIOEventQueue) << 1);
      
  return 0;
}

int mainTask(const void * pvEvtData, void * pvOutEvtData) {

  // Convert the ThreadMsg void* data to a sCSPEvent_t* 
  const sCSPEvent_t* cspEvtData = static_cast<const sCSPEvent_t*>(pvEvtData);

  // Get standard packet and connection pointers
  csp_packet_t *packet = static_cast<csp_packet_t*>(cspEvtData->pvData);
  csp_conn_t *conn = cspEvtData->optConn;

  uint8_t command = packet->data[0];
  //uint8_t * packetPayload = packet->data+1;
  
  switch(cspEvtData->eEventType) {
    // sends the info about the system
    case directEvent:
    {
      //csp_close(conn); // close connection, reply only with packet
      switch(command)
      {
	case XRAY_DK_CREATE_STORAGES: {
	  csp_close(conn);
	  if (createStorages() == 1)
	    replyPacketOk(packet);
	  else
	    replyPacketErr(packet, ERROR_STORAGES_NOT_CREATED);
	  break;
	}
	case XRAY_SYSTEM_SHUTDOWN: {
          csp_close(conn);
          //int rv = xray_system_shutdown();
	  // forward to detectorTask
	  detectorEvent_t * detEvt = new detectorEvent_t; // deleted by handler
	  detEvt->eventType = DETECTOR_SYSTEM_SHUTDOWN;
          detEvt->pvData = NULL;
          //int ierr, rv = -1; // rv deleted here
	  //int rv = xray_system_shutdown();
	  sDetectorEventQueue.PostMsg(detEvt);
	  int rv = 0;
          if(rv)
            replyPacketErr(packet, rv);
          else
            replyPacketOk(packet);
          break;
        }
        case XRAY_DETECTOR_INIT: {
	  csp_close(conn);
	  // forward to detectoTask
          detectorEvent_t * detEvt = new detectorEvent_t; // deleted by handler                                                                                                                                                              
          detEvt->eventType = DETECTOR_INIT;
          detEvt->pvData = NULL;
          int ierr, rv = -1; // rv deleted here                                                                                                                                                                                              
          ierr = sDetectorEventQueue.PostMsg(detEvt, 1000, &rv, sizeof(rv));
	  if(rv)
	    replyPacketErr(packet, rv);
          else
            replyPacketOk(packet);
	  break;
        }
        case XRAY_DETECTOR_SET_ACQ_MODE: {
          csp_close(conn);
	  xray_cmd_single_int_t * cmd_packet = (xray_cmd_single_int_t*)(packet->data);
	  detectorEvent_t * detEvt = new detectorEvent_t; // deleted by handler                                                                                                                                                              
          detEvt->eventType = DETECTOR_SET_ACQ_MODE;
          detEvt->pvData = new int(cmd_packet->arg0); // deleted by handler
	  g_ex_settings.detector.mode = cmd_packet->arg0;
          int ierr, rv = -1; // rv deleted here
          ierr = sDetectorEventQueue.PostMsg(detEvt, 1000, &rv, sizeof(rv));
          if(rv)
            replyPacketErr(packet, rv);
          else
            replyPacketOk(packet);
          break;
        }
        case XRAY_DETECTOR_SET_BIAS: {
          csp_close(conn);
          xray_cmd_single_dbl_t * cmd_packet = (xray_cmd_single_dbl_t*)(packet->data);
          detectorEvent_t * detEvt = new detectorEvent_t; // deleted by handler                                                                                                                                                              
          detEvt->eventType = DETECTOR_SET_BIAS;
          detEvt->pvData = new double(cmd_packet->arg0); // deleted by handler
	  g_ex_settings.detector.bias =	cmd_packet->arg0;
          int ierr, rv = -1; // rv deleted here                                                                                                                                                                                              
          ierr = sDetectorEventQueue.PostMsg(detEvt, 1000, &rv, sizeof(rv));
          if(rv)
            replyPacketErr(packet, rv);
          else
            replyPacketOk(packet);
          break;
	}
        case XRAY_DETECTOR_SET_THRESHOLD: {
          csp_close(conn);
          xray_cmd_single_dbl_t * cmd_packet = (xray_cmd_single_dbl_t*)(packet->data);
          detectorEvent_t * detEvt = new detectorEvent_t; // deleted by handler                                                                                                                                                              
          detEvt->eventType = DETECTOR_SET_THRESHOLD;
          detEvt->pvData = new double(cmd_packet->arg0); // deleted by handler
	  g_ex_settings.detector.threshold = cmd_packet->arg0;
          int ierr, rv = -1; // rv deleted here                                                                                                                                                                                              
          ierr = sDetectorEventQueue.PostMsg(detEvt, 1000, &rv, sizeof(rv));
          if(rv)
            replyPacketErr(packet, rv);
          else
            replyPacketOk(packet);
          break;
	}
        case XRAY_DETECTOR_SET_EXPOSURE: {
          csp_close(conn);
          xray_cmd_single_int_t * cmd_packet = (xray_cmd_single_int_t*)(packet->data);
          int rv = xray_detector_set_exposure(cmd_packet->arg0);
          if(rv)
            replyPacketErr(packet, rv);
          else
            replyPacketOk(packet);
          break;
	}
	case XRAY_DETECTOR_SET_PERIOD: {
          csp_close(conn);
          xray_cmd_single_int_t * cmd_packet = (xray_cmd_single_int_t*)(packet->data);
          int rv = xray_detector_set_period(cmd_packet->arg0);
          if(rv)
            replyPacketErr(packet, rv);
          else
            replyPacketOk(packet);
          break;
        }
	case XRAY_DETECTOR_SET_NFRAMES: {
          csp_close(conn);
          xray_cmd_single_int_t * cmd_packet = (xray_cmd_single_int_t*)(packet->data);
          int rv = xray_detector_set_nframes(cmd_packet->arg0);
          if(rv)
            replyPacketErr(packet, rv);
          else
            replyPacketOk(packet);
          break;
        }
	case XRAY_DETECTOR_SET_ROI: {
          csp_close(conn);
          xray_cmd_setroi_t * cmd_packet = (xray_cmd_setroi_t*)(packet->data);
          int rv = xray_detector_set_roi(cmd_packet->id, cmd_packet->row, cmd_packet->col, cmd_packet->height, cmd_packet->width);
          if(rv)
            replyPacketErr(packet, rv);
          else
            replyPacketOk(packet);
          break;
        }
        case XRAY_SET_OUTPUTFORM: {
          csp_close(conn);
          xray_cmd_single_int_t * cmd_packet = (xray_cmd_single_int_t*)(packet->data);
          int rv = xray_set_outputform(cmd_packet->arg0);
          if(rv)
            replyPacketErr(packet, rv);
          else
            replyPacketOk(packet);
          break;
        }
	case XRAY_SET_FILTERING: {
          csp_close(conn);
          xray_cmd_single_int_t * cmd_packet = (xray_cmd_single_int_t*)(packet->data);
          int rv = xray_set_filtering(cmd_packet->arg0);
          if(rv)
            replyPacketErr(packet, rv);
          else
            replyPacketOk(packet);
          break;
        }
	case XRAY_SET_ENERGY_HISTOGRAM_LIMIT: {
          csp_close(conn);
          xray_cmd_single_int_t * cmd_packet = (xray_cmd_single_int_t*)(packet->data);
          int rv = xray_set_energy_histogram_limit(cmd_packet->arg0);
          if(rv)
            replyPacketErr(packet, rv);
          else
            replyPacketOk(packet);
          break;
        }
	case XRAY_SET_COUNT_TRESHOLD: {
          csp_close(conn);
          xray_cmd_setcount_t * cmd_packet = (xray_cmd_setcount_t*)(packet->data);
          int rv = xray_set_count_treshold(cmd_packet->treshold, cmd_packet->id);
          if(rv)
            replyPacketErr(packet, rv);
          else
            replyPacketOk(packet);
          break;
        }
	case XRAY_DETECTOR_SET_PARAMETERS: {
          csp_close(conn);
          xray_cmd_setparam_t * cmd_packet = (xray_cmd_setparam_t*)(packet->data);
	  int rv = 0;
	  rv = xray_set_parameters(cmd_packet);
          if(rv)
            replyPacketErr(packet, rv);
          else
            replyPacketOk(packet);
          break;
        }
	case XRAY_DETECTOR_MEASURE: {
          csp_close(conn);
          xray_cmd_t * cmd_packet = (xray_cmd_t*)(packet->data);
	  xray_measure(SCAN_MODE_SIMPLE, false);
          replyPacketOk(packet);
          break;
        }
	case XRAY_DETECTOR_MEASURE_SCAN: {
          csp_close(conn);
          xray_cmd_t * cmd_packet = (xray_cmd_t*)(packet->data);
          xray_measure(SCAN_MODE_COUNTING, false);
	  replyPacketOk(packet);
          break;
        }
        case XRAY_DETECTOR_MEASURE_SHUTDOWN: {
          csp_close(conn);
          xray_cmd_t * cmd_packet = (xray_cmd_t*)(packet->data);
          xray_measure(SCAN_MODE_SIMPLE, true);
          replyPacketOk(packet);
          break;
        }
        case XRAY_DETECTOR_MEASURE_SCAN_SHUTDOWN: {
          csp_close(conn);
          xray_cmd_t * cmd_packet = (xray_cmd_t*)(packet->data);
          xray_measure(SCAN_MODE_COUNTING, true);
          replyPacketOk(packet);
          break;
        }
	case XRAY_EXPERIMENT_SAVE_CONFIG: {
          csp_close(conn);
          xray_cmd_t * cmd_packet = (xray_cmd_t*)(packet->data);
	  int rv = xray_save_config(true);
          if(rv)
            replyPacketErr(packet, rv);
          else
            replyPacketOk(packet);
          break;
	}
        case XRAY_EXPERIMENT_RESET_CONFIG: {
          csp_close(conn);
          xray_cmd_t * cmd_packet = (xray_cmd_t*)(packet->data);
          int rv = xray_save_config(false);
          if(rv)
            replyPacketErr(packet, rv);
          else
            replyPacketOk(packet);
          break;
        }
	case XRAY_EXPERIMENT_LOAD_CONFIG: {
          csp_close(conn);
          xray_cmd_t * cmd_packet = (xray_cmd_t*)(packet->data);
          int rv = load_experiment_config_dk(false);
          if(rv)
            replyPacketErr(packet, rv);
          else
            replyPacketOk(packet);
          break;
        }
	case XRAY_SET_AUTOEXEC: {
          csp_close(conn);
	  xray_cmd_autoexec_t * cmd_packet = (xray_cmd_autoexec_t*)(packet->data);
          int rv = xray_set_autoexec(cmd_packet->autoexec, cmd_packet->poweroff);
          if(rv)
            replyPacketErr(packet, rv);
          else
            replyPacketOk(packet);
          break;
        }
	case XRAY_GET_STATUS: {
          csp_close(conn);
	  xray_cmd_t * cmd_packet = (xray_cmd_t*)(packet->data);
	  xray_status_t status;
          int rv = xray_get_status(status);
          if(rv)
            replyPacketErr(packet, rv);
          else
            replyPacketStatus(packet, status);
          break;
	}
	case XRAY_GET_STATUS_DK: {
          csp_close(conn);
          xray_cmd_t * cmd_packet = (xray_cmd_t*)(packet->data);
          xray_status_t status;
          int rv = xray_get_status(status);
	  xray_send_status(status); // ignore rv (IO timeout)
          if(rv)
            replyPacketErr(packet, rv);
          else
            replyPacketOk(packet);
          break;
        }
	case XRAY_DETECTOR_SLEEP: {
          csp_close(conn);
          xray_cmd_single_int_t * cmd_packet = (xray_cmd_single_int_t*)(packet->data);
          int rv = xray_detector_sleep(cmd_packet->arg0);
          if(rv)
            replyPacketErr(packet, rv);
          else
            replyPacketOk(packet);
          break;
        }
        default: {
	  /* Should not get here. */
	  break;
	}
      } // switch(command)
    } // case direct event
    default: {
      /* Should not get here. */
      break; // default
    }
  } // switch(eventType)
  						
  // Delete dynamic data passed through message queue
  delete cspEvtData;
  return 0;
}
