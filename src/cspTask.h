/*
 * cspTask.h
 *
 *  Author: Tomas Baca, Zdenek Matej
 */ 

//#include "system.h"
#include "csp/csp.h"

#ifndef _CSPTASK_H_
#define _CSPTASK_H_

typedef enum {

 directEvent = 0,
 obcEvent = 1,
	
} sCSPEventType_t;

typedef struct CSP_TASK_EVT_DTA
{
  sCSPEventType_t eEventType; /* Tells the receiving task what the event is. */
  void *pvData; /* Holds or points to any data associated with the event. */
  csp_conn_t *optConn; /* Connection (must be closed by handler if not NULL). */
  
} sCSPEvent_t;

#ifdef __cplusplus
extern "C" {
#endif
      
/* -------------------------------------------------------------------- */
/*	Task that handles CSP incoming packets				*/
/* -------------------------------------------------------------------- */
void * cspTask(void *p);

/* -------------------------------------------------------------------- */
/*      Synchronize time with node                                      */
/* -------------------------------------------------------------------- */  
int synchronize_time(int node, bool csp, bool system);
  
#ifdef __cplusplus
}
#endif

#ifdef __cplusplus

#include "WorkerThread.h"

extern WorkerThread sDetectorEventQueue;
extern WorkerThread sIOEventQueue;

typedef struct __attribute__((packed)) GLOBAL_SETTINGS
{
  uint32_t version = 0;
  unsigned long start_nb = 0;
} global_settings_t;

global_settings_t get_global_app_settings();

int xcsp_buf_sendto(uint8_t    prio,
                    uint8_t    dest,
                    uint8_t    dport,
                    uint8_t    src_port,
                    uint32_t   opts,
                    void     * outbuf,
                    int        outlen,
                    uint32_t   timeout);

#include "ConsumableValue.h"
extern ConsumableValue<int32_t> sCSPAckConsumable;
extern ConsumableValue<uint32_t> sCSPDKInfoAckConsumable;

#endif /* __cplusplus */

#endif /* _CSPTASK_H_ */
