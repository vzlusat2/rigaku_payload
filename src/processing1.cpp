#include <cstring>
#include <exception>
#include <limits>
#include <string>

#include "Log.h"
#include "processing1.h"

int new_filtering_workspace_cluster1(filtering_workspace_cluster1_t & workspace)
{
  workspace.frame_sz = DET_FRAME_SZ;
  workspace.height = DET_HEIGHT;
  workspace.width = DET_WIDTH;
  workspace.cluster_sz = 0;
  workspace.cluster_row_low = std::numeric_limits<FRAME_DIM_TYPE>::max();
  workspace.cluster_row_hgh = std::numeric_limits<FRAME_DIM_TYPE>::min();
  workspace.cluster_col_low = std::numeric_limits<FRAME_DIM_TYPE>::max();
  workspace.cluster_col_hgh = std::numeric_limits<FRAME_DIM_TYPE>::min();
  
  try {
    workspace.processed = new bool[DET_FRAME_SZ]();
    workspace.cluster_rows = new FRAME_DIM_TYPE[DET_FRAME_SZ]();
    workspace.cluster_cols = new FRAME_DIM_TYPE[DET_FRAME_SZ]();
    workspace.cluster_data = new FRAME_DATA_TYPE[DET_FRAME_SZ]();
  } catch (std::exception & e) {
    write_log(LOG_ERROR, "Error when allocating memory for cluster filtering workspace");
    return 1;
  }
  
  return 0;
}

int delete_filtering_workspace_cluster1(filtering_workspace_cluster1_t & workspace)
{
  workspace.cluster_sz = 0;
  workspace.cluster_row_low = std::numeric_limits<FRAME_DIM_TYPE>::max();
  workspace.cluster_row_hgh = std::numeric_limits<FRAME_DIM_TYPE>::min();
  workspace.cluster_col_low = std::numeric_limits<FRAME_DIM_TYPE>::max();
  workspace.cluster_col_hgh = std::numeric_limits<FRAME_DIM_TYPE>::min();
  
  try {
    if(workspace.processed) {
      delete [] workspace.processed;
      workspace.processed = NULL;
    }
    if(workspace.cluster_rows) {
      delete [] workspace.cluster_rows;
      workspace.cluster_rows = NULL;
    }
    if(workspace.cluster_cols) {
      delete [] workspace.cluster_cols;
      workspace.cluster_cols = NULL;
    }
    if(workspace.cluster_data) {
      delete [] workspace.cluster_data;
      workspace.cluster_data = NULL;
    }
  } catch (std::exception & e) {
    write_log(LOG_ERROR, "Error when releasing memory for cluster filtering workspace");
    return 1;
  }

  return 0;
}

int process_pixel1(size_t ipix, FRAME_DATA_TYPE val, const FRAME_DATA_TYPE * frame, filtering_workspace_cluster1_t & wsp, unsigned int recursion_level)
{
  size_t row = ipix / wsp.width;
  size_t col = ipix % wsp.width;

  int ncluster = 1;
  
  // add pixel to cluster
  wsp.cluster_rows[wsp.cluster_sz] = row;
  wsp.cluster_cols[wsp.cluster_sz] = col;
  wsp.cluster_data[wsp.cluster_sz] = val;
  if(row>wsp.cluster_row_hgh)
    wsp.cluster_row_hgh = row;
  if(row<wsp.cluster_row_low)
    wsp.cluster_row_low = row;
  if(col>wsp.cluster_col_hgh)
    wsp.cluster_col_hgh = col;
  if(col<wsp.cluster_col_low)
    wsp.cluster_col_low = col;
  // set pixel as processed
  wsp.processed[ipix] = true;
  // increase cluster size
  wsp.cluster_sz++;
  // process neighbouring pixels
  // upper-left
  if((row>0) && (col>0)) {
    size_t inext = (row-1)*wsp.width + (col-1);
    if(!wsp.processed[inext]) {
      FRAME_DATA_TYPE nextval = frame[inext];
      if(nextval) {
	ncluster += process_pixel1(inext, nextval, frame, wsp, recursion_level+1);
      }
    } 
  }
  // upper
  if((row>0)) {
    size_t inext = (row-1)*wsp.width + (col);
    if(!wsp.processed[inext]) {
      FRAME_DATA_TYPE nextval = frame[inext];
      if(nextval) {
        ncluster += process_pixel1(inext, nextval, frame, wsp, recursion_level+1);
      }
    }
  }
  // upper-right
  if((row>0) && (col<(wsp.width-1))) {
    size_t inext = (row-1)*wsp.width + (col+1);
    if(!wsp.processed[inext]) {
      FRAME_DATA_TYPE nextval = frame[inext];
      if(nextval) {
        ncluster += process_pixel1(inext, nextval, frame, wsp, recursion_level+1);
      }
    }
  }
  // left
  if((col>0)) {
    size_t inext = (row)*wsp.width + (col-1);
    if(!wsp.processed[inext]) {
      FRAME_DATA_TYPE nextval = frame[inext];
      if(nextval) {
        ncluster += process_pixel1(inext, nextval, frame, wsp, recursion_level+1);
      }
    }
  }
  // right
  if((col<(wsp.width-1))) {
    size_t inext = (row)*wsp.width + (col+1);
    if(!wsp.processed[inext]) {
      FRAME_DATA_TYPE nextval = frame[inext];
      if(nextval) {
        ncluster += process_pixel1(inext, nextval, frame, wsp, recursion_level+1);
      }
    }
  }
  // bottom-left
  if((row<(wsp.height-1)) && (col>0)) {
    size_t inext = (row+1)*wsp.width + (col-1);
    if(!wsp.processed[inext]) {
      FRAME_DATA_TYPE nextval = frame[inext];
      if(nextval) {
        ncluster += process_pixel1(inext, nextval, frame, wsp, recursion_level+1);
      }
    }
  }
  // bottom
  if((row<(wsp.height-1))) {
    size_t inext = (row+1)*wsp.width + (col);
    if(!wsp.processed[inext]) {
      FRAME_DATA_TYPE nextval = frame[inext];
      if(nextval) {
        ncluster += process_pixel1(inext, nextval, frame, wsp, recursion_level+1);
      }
    }
  }
  // bottom-right
  if((row<(wsp.height-1)) && (col<(wsp.width-1))) {
    size_t inext = (row+1)*wsp.width + (col+1);
    if(!wsp.processed[inext]) {
      FRAME_DATA_TYPE nextval = frame[inext];
      if(nextval) {
        ncluster += process_pixel1(inext, nextval, frame, wsp, recursion_level+1);
      }
    }
  }
  
  return ncluster;
}

int calc_filtered_cluster1(FRAME_DIM_TYPE & row, FRAME_DIM_TYPE & col, FRAME_DATA_TYPE & tot, bool use_calib, int det_mode, const filtering_workspace_cluster1_t & wsp)
{
  float frow = 0;
  float fcol = 0;
  float en = 0.;

  for(size_t i=0; i<wsp.cluster_sz; i++) {
    FRAME_DIM_TYPE irow = wsp.cluster_rows[i];
    FRAME_DIM_TYPE icol = wsp.cluster_cols[i];
    size_t ipix = irow*wsp.width + icol;
    float pix_en = tot_to_energy(ipix, wsp.cluster_data[i], use_calib);
    en += pix_en;
    frow += irow*pix_en;
    fcol += icol*pix_en;
  }

  int rv = 0;
  
  // center of mass pixel
  if(en>0.) {
    frow /= en;
    fcol /= en;
    if(((size_t)(frow+0.5f)<0) || ((size_t)(frow+0.5f)>=wsp.height)) {
      row = 0;
      rv = 1;
    } else
      row = (FRAME_DIM_TYPE)(frow+0.5f);
    if(((size_t)(fcol+0.5f)<0) || ((size_t)(fcol+0.5f)>=wsp.width)) {
      col = 0;
      rv = 1;
    } else
      col = (FRAME_DIM_TYPE)(fcol+0.5f);
    if(rv==0) {
      size_t ipix = row*wsp.width + col;
      tot = energy_to_tot(ipix, en, use_calib);
    } else
      tot = 0;
  } else {
    row = 0;
    col = 0;
    tot = 0;
    rv = 1;
  }

  if(det_mode==0)
    tot /= wsp.cluster_sz; // counting mode normalization
  
  return rv;
}
			    
int filter_cluster_events1(FRAME_DATA_TYPE * filtered, const FRAME_DATA_TYPE * frame,
			   int cluster_dim,
			   bool use_calib,
			   int det_mode,
			   size_t height, size_t width,
			   filtering_workspace_cluster1_t & wsp)
{
  int cluster_area = cluster_dim*cluster_dim;
  
  // clear workspace and filtered image
  wsp.cluster_sz = 0;
  wsp.cluster_row_low = std::numeric_limits<FRAME_DIM_TYPE>::max();
  wsp.cluster_row_hgh = std::numeric_limits<FRAME_DIM_TYPE>::min();
  wsp.cluster_col_low = std::numeric_limits<FRAME_DIM_TYPE>::max();
  wsp.cluster_col_hgh = std::numeric_limits<FRAME_DIM_TYPE>::min();
  bool *pb = wsp.processed;
  FRAME_DATA_TYPE *pfilt = filtered;
  for(size_t ipix=0; ipix<wsp.frame_sz; ipix++) {
    *pb = false;
    *pfilt = (FRAME_DATA_TYPE)0;
    pb++;
    pfilt++;
  }
  
  // process all nonzero pixels
  const FRAME_DATA_TYPE *pframe = frame;
  pb = wsp.processed;
  for(size_t ipix=0; ipix<wsp.frame_sz; ipix++) {
    // zero or processed
    if((*pframe>0) && (!(*pb))) {
      // new cluster
      wsp.cluster_sz = 0;
      wsp.cluster_row_low = std::numeric_limits<FRAME_DIM_TYPE>::max();
      wsp.cluster_row_hgh = std::numeric_limits<FRAME_DIM_TYPE>::min();
      wsp.cluster_col_low = std::numeric_limits<FRAME_DIM_TYPE>::max();
      wsp.cluster_col_hgh = std::numeric_limits<FRAME_DIM_TYPE>::min();
      // process pixel
      int ncluster = process_pixel1(ipix, *pframe, frame, wsp, 0);
      if(ncluster && (wsp.cluster_sz<=cluster_area) && ((wsp.cluster_row_hgh-wsp.cluster_row_low+1)<=cluster_dim) && ((wsp.cluster_col_hgh-wsp.cluster_col_low+1)<=cluster_dim)) {
	FRAME_DIM_TYPE filt_row;
	FRAME_DIM_TYPE filt_col;
	FRAME_DATA_TYPE filt_tot;
	// process cluster
	int rv = calc_filtered_cluster1(filt_row, filt_col, filt_tot, use_calib, det_mode, wsp);
	/*write_log(LOG_INFO, "(%ld,%ld,%d) cluster-size: %d, %d x %d, %d, %d, %d, %d", ipix / DET_WIDTH, ipix % DET_WIDTH, *pframe, wsp.cluster_sz,
		  wsp.cluster_row_hgh-wsp.cluster_row_low+1,
		  wsp.cluster_col_hgh-wsp.cluster_col_low+1,
		  rv, filt_row, filt_col, filt_tot);*/
	// add cluster to filtered image
	if(rv==0) {
	  size_t filt_ipix = filt_row * wsp.width + filt_col;
	  filtered[filt_ipix] = filt_tot;
	}
      }
    }
    pframe++;
    pb++;
  }
  
  return 0;
}
