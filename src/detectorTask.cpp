/* -------------------------------------------------------------------- */
/*	Detector task							*/
/* -------------------------------------------------------------------- */

#include <csp/csp.h>

#include "xray.h"
#include "detectorTask.h"
#include "mainTask.h"
#include "ioTask.h"
#include "cspTask.h"
#include "errorCodes.h"
#include "Log.h"
#include "processing.h"
#include "processing1.h"

#include <csp/csp_cmp.h>
#include <csp/csp_endian.h>

#include <unistd.h>

#include "pxcapi.h"

using namespace std;

#define IMILLION   1000000
#define ITHOUSAND  1000

// forward function decleration ----------
int xray_detector_init();
int xray_detector_set_acq_mode(int mode);
int xray_detector_set_bias(double bias);
int xray_detector_set_threshold(double threshold);
int xray_detector_measure(const experiment_settings_t & expset);
int xray_detector_update(const experiment_settings_t & expset);

void send_frame_events(const frame_data_t & data, uint16_t id, bool filtered, const roi_settings_t & roi, bool single_roi);
void send_energy_histograms(const frame_data_t & data, const roi_settings_t rois[], const size_t nrois, uint16_t en_max, uint16_t id);
void send_histograms(const frame_data_t & data, const roi_settings_t rois[], const size_t nrois, uint16_t id);
void send_binned_data(const frame_data_t & data, uint16_t outputform, uint16_t id);
void start_io_session();
void end_io_session();
void send_metainfo(const experiment_metainfo_t & info);
// ---------------------------------------

// global initialization ----------
float *g_det_caliba = NULL;
float *g_det_calibb = NULL;
float *g_det_calibc = NULL;
float *g_det_calibt = NULL;
bool  g_det_calib_avail = false;
// ---------------------------------------

int detectorTask(const void * taskParam, void * taskOutput)
{
  // Convert the ThreadMsg void* data to a detectorEvent_t*                                                                                                                                                                                  
  const detectorEvent_t* detEvt = static_cast<const detectorEvent_t*>(taskParam);
  
  switch(detEvt->eventType) {
    case DETECTOR_TASK_VOID: {
      int *prv = (int*) taskOutput;
      *prv = 0;
      break;
    }
    case DETECTOR_INIT: {
      int *prv = (int*) taskOutput;
      *prv = xray_detector_init();
      break;
    }
    case DETECTOR_SET_ACQ_MODE: {
      int *pmode = (int*) detEvt->pvData;
      int *prv = (int*) taskOutput;
      *prv = xray_detector_set_acq_mode(*pmode);
      delete pmode;
      break;
    }
    case DETECTOR_SET_BIAS: {
      double *pbias = (double*) detEvt->pvData;
      int *prv = (int*) taskOutput;
      *prv = xray_detector_set_bias(*pbias);
      delete pbias;
      break;
    }
    case DETECTOR_SET_THRESHOLD: {
      double *pthreshold = (double*) detEvt->pvData;
      int *prv = (int*) taskOutput;
      *prv = xray_detector_set_threshold(*pthreshold);
      delete pthreshold;
      break;
    }
    case DETECTOR_MEASURE: {
      experiment_settings_t *pExpConf = (experiment_settings_t*) detEvt->pvData;
      // check & override outputform energy calibration flag
      if(!g_det_calib_avail && (pExpConf->detector.mode==PXC_TPX_MODE_TOT) && ((pExpConf->outputform & XRAY_OUTPUTFORM_ECALIB_DISABLED)==0))
	pExpConf->outputform |= XRAY_OUTPUTFORM_ECALIB_DISABLED;
      xray_detector_measure(*pExpConf);
      delete pExpConf;
      break;
    }
    case DETECTOR_UPDATE: {
      experiment_settings_t *pExpConf = (experiment_settings_t*) detEvt->pvData;
      xray_detector_update(*pExpConf);
      delete pExpConf;
      break;
    }
    case DETECTOR_SYSTEM_SHUTDOWN: {
      // forward to ioTask                                                                                                                                                                                                          
      ioEvent_t * ioEvt = new ioEvent_t; // deleted by handler                                                                                                                                                              
      ioEvt->eventType = XRAY_IO_SYSTEM_SHUTDOWN;
      ioEvt->pvData = NULL;
      sIOEventQueue.PostMsg(ioEvt);	  
      break;
    }
    case DETECTOR_SLEEP: {
      uint32_t *pmsec = (uint32_t*) detEvt->pvData;
      write_log(LOG_INFO, "Detector sleeping for %u (msec)", *pmsec); 
      usleep((*pmsec)*1000);
      delete pmsec;
      break;
    }
    default: {
      break;
    }
  }
  
  // Delete dynamic data passed through message queue                                                                                                                                                                                        
  delete detEvt;
  return 0;
}

int xray_detector_init()
{
  // Synchronize time with OBC_NODE
  synchronize_time(NODE_OBC, true, true);
  
  // Refresh Pixet devices
  int rc = pxcRefreshDevices();
  if (rc) {
    write_log(LOG_ERROR, "Pixet: Could not refresh devices, error=%d", rc);
    return ERROR_DETECTOR_INIT_INIT;
  }

  int connectedDevicesCount = pxcGetDevicesCount();
  write_log(LOG_INFO, "Connected devices: %d", connectedDevicesCount);

  if (connectedDevicesCount == 0) {
    return ERROR_DETECTOR_INIT_NUMDEVICES;
  }

  char name[1024];
  pxcGetDeviceName(DET_DEV_IDX, name, 1024);
  write_log(LOG_INFO, "Connected Device: %s", name);

  // Detector serial number
  string nstr(name);
  try {
    nstr = nstr.substr(nstr.find_last_of(" ")+1);
  } catch (exception& e) {
    write_log(LOG_ERROR, "Exception: %s", e.what());
    nstr = string("C07-W0337"); // set default (vzlusta CdTe flight)
  }

  // Load config
  string str = "/opt/rite/configs/MiniPIX-" + nstr + ".xml";
  rc = pxcLoadDeviceConfiguration(DET_DEV_IDX, str.c_str());
  if (rc) {
    write_log(LOG_ERROR, "Error when loading detector configuration: %s", str.c_str());
    return ERROR_DETECTOR_INIT_CONFIG;
  }

  // Check energy calibration settings
  rc = pxcSetTimepixCalibrationEnabled(DET_DEV_IDX, false);
  /*rc = pxcSetTimepixCalibrationEnabled(DET_DEV_IDX, true);
  if (rc) {
    write_log(LOG_ERROR, "Error when enabling detector energy calibration");
    return ERROR_DETECTOR_INIT_ENERGY_CALIBRATION;
  }*/
  write_log(LOG_INFO, "Calibration enabled: %d", pxcIsTimepixCalibrationEnabled(DET_DEV_IDX));

  // Load detector calibration matrices
  rc = load_detector_calibration(nstr.c_str());
  if(rc) {
    write_log(LOG_WARNING, "Error when loading detector calibration matrices, detector calibration will be disabled");
  }
   
  write_log(LOG_INFO, "Detector initialized");

  return 0;
}

int xray_detector_set_acq_mode(int mode)
{
  int rc = pxcSetTimepixMode(DET_DEV_IDX, mode);
  if (rc) {
    write_log(LOG_ERROR, "Error setting dector mode: %d, err=%d", mode, rc);
    return ERROR_DETECTOR_SET_ACQ_MODE;
  }

  write_log(LOG_INFO, "Detector mode=%d", pxcGetTimepixMode(DET_DEV_IDX));

  return 0;
}

int xray_detector_set_bias(double bias)
{
  int rc = pxcSetBias(DET_DEV_IDX, bias);
  if (rc) {
    write_log(LOG_ERROR, "Error setting dector bias: %g (V), err=%d", bias, rc);
    return ERROR_DETECTOR_SET_BIAS;
  }

  double val;
  pxcGetBias(DET_DEV_IDX, &val);
  write_log(LOG_INFO, "Detector bias=%g (V)", val);

  return 0;
}

int xray_detector_set_threshold(double threshold)
{
  int rc = pxcSetThreshold(DET_DEV_IDX, 0, threshold);
  if (rc) {
    write_log(LOG_ERROR, "Error setting dector threshold: %g (keV), err=%d", threshold, rc);
    return ERROR_DETECTOR_SET_THRESHOLD;
  }

  double val;
  pxcGetThreshold(DET_DEV_IDX, 0, &val);
  write_log(LOG_INFO, "Detector threshold: %g (keV)", val);

  return 0;
}

int xray_detector_measure(const experiment_settings_t & expset)
{
  write_log(LOG_INFO, "Measure: m=%d, tm=%g (s), n=%d, expn=%d, sn=%d, f=%d, s=%d",
	    expset.detector.mode, expset.detector.frameTime, expset.detector.nFrames,
	    expset.exp_nb, expset.application.start_nb, expset.filtering, expset.scan_mode);

  int rc = 0;

  if(pxcGetTimepixMode(DET_DEV_IDX)!=expset.detector.mode)
    xray_detector_set_acq_mode(expset.detector.mode);

  unsigned frameSize = DET_FRAME_SZ;
  frame_data_t data;
  new_frame_data(data, expset.detector.rois);
  FRAME_DATA_TYPE * buf_frame = new FRAME_DATA_TYPE[DET_FRAME_SZ]();
  FRAME_DATA_TYPE * buf_filtered = new FRAME_DATA_TYPE[DET_FRAME_SZ]();
  filtering_workspace_cluster1_t cluster_wsp;
  if(expset.filtering>4)
    new_filtering_workspace_cluster1(cluster_wsp);

  bool use_calib = g_det_calib_avail && (expset.detector.mode==PXC_TPX_MODE_TOT) && ((expset.outputform & XRAY_OUTPUTFORM_ECALIB_DISABLED)==0);
  
  // --- experiment metainfo ---
  experiment_metainfo_t expinfo;
  
  // application
  expinfo.start_nb = expset.application.start_nb;
  // detector
  expinfo.frameTime = expset.detector.frameTime;
  expinfo.nFrames = 0; // updated later
  expinfo.mode = pxcGetTimepixMode(DET_DEV_IDX);
  double _bias;
  pxcGetBias(DET_DEV_IDX, &(_bias));
  expinfo.bias = _bias;
  double _threshold;
  pxcGetThreshold(DET_DEV_IDX, 0, &(_threshold));
  expinfo.threshold = _threshold;
  for(size_t iroi=0; iroi<DET_NROIS; iroi++) {
    expinfo.rois[iroi][0] = expset.detector.rois[iroi].row;
    expinfo.rois[iroi][1] = expset.detector.rois[iroi].col;
    expinfo.rois[iroi][2] = expset.detector.rois[iroi].height;
    expinfo.rois[iroi][3] = expset.detector.rois[iroi].width;
    //expinfo.rois[iroi][4] = expset.detector.rois[iroi].method;
  }
  // experiment
  expinfo.exp_nb = expset.exp_nb;
  expinfo.outputForm = expset.outputform;
  expinfo.scanMode = expset.scan_mode;
  expinfo.filtering = expset.filtering;
  expinfo.count_roi = expset.count_roi;
  expinfo.count_threshold = expset.count_threshold;
  
  struct timespec tstart, tend;
  long uperiod = 0;

  if((expset.scan_mode!=SCAN_MODE_COUNTING) && (abs(expset.detector.framePeriod)>1e-9)) {
    uperiod = (long)(expset.detector.framePeriod*IMILLION);
  }
  
  clock_gettime(CLOCK_REALTIME, &tstart);
	
  // ------------------------
  bool stop_condition = false;
  // ------------------------
  
  // frame by frame
  for(int iframe=0; iframe<expset.detector.nFrames; iframe++) {

    bool frame_suc = false;

    if(uperiod) {
      struct timespec tnow;
      clock_gettime(CLOCK_REALTIME, &tnow);
      
      long usleep_tm = 10*ITHOUSAND + iframe*uperiod - (tnow.tv_sec - tstart.tv_sec)*IMILLION - (tnow.tv_nsec - tstart.tv_nsec) / ITHOUSAND; /* 10 ms delay */
      if(usleep_tm>0)
	usleep((useconds_t)usleep_tm);
    }
    
    if((expset.scan_mode!=SCAN_MODE_COUNTING) || (iframe==0)) {
      // time
      csp_timestamp_t ts;
      csp_clock_get_time(&ts);
      expinfo.tv_sec = ts.tv_sec;
      expinfo.tv_nsec = ts.tv_nsec;
    }

    // --- measure and get data ---
    if(expset.scan_mode==SCAN_MODE_COUNTING) {
      rc = pxcMeasureSingleFrame(DET_DEV_IDX, expset.detector.frameTime,
				 buf_frame, &frameSize, PXC_TRG_NO);
    } else {
      rc = pxcMeasureSingleFrame(DET_DEV_IDX, expset.detector.frameTime,
                                 data.frame, &frameSize, PXC_TRG_NO);
    }
    
    // --- error check ---
    if(rc) {
      write_log(LOG_ERROR, "Error when measuring frame nb=%d, rc=%d", iframe, rc);
      frame_suc = false;
      stop_condition = true;
      if(expset.scan_mode!=SCAN_MODE_COUNTING)
	break;
    } else {
      frame_suc = true;
      stop_condition |= iframe==(expset.detector.nFrames-1); // the last frame
    }

    if((expset.scan_mode==SCAN_MODE_COUNTING) && frame_suc) {
      // add frame
      FRAME_DATA_TYPE * pval = buf_frame;
      FRAME_DATA_TYPE * psum = data.frame;
      for(size_t i=0; i<DET_FRAME_SZ; i++) {
	*psum += *pval;
	psum++; pval++;
      }
    }
      
    // --- save frame (DEBUG) ---
    //pxcSaveMeasuredFrame(DET_DEV_IDX, 0, "/tmp/test.png");
    //write_log(LOG_INFO, "Saved to /tmp/test.png");

    //save_file_bin(data.frame, data.frame_sz, "/tmp/test_frameData_ushort.dat");

    // --- process data ---
    
    // --- frame preprocessing ---
    if(expset.outputform & XRAY_OUTPUTFORM_FRAME_MASKED2) {
      uint16_t mask_threshold = 11809;

      if(expset.scan_mode==SCAN_MODE_COUNTING) { // counting
	if(frame_suc)
          mask_above(buf_frame, buf_frame, data.height*data.width, mask_threshold);
      } else // not counting
	mask_above(data.frame, data.frame, data.height*data.width, mask_threshold);
    }
    
    if((expset.outputform & XRAY_OUTPUTFORM_FRAME_MASKED) && (expset.en_hist_limit>0)) {
      uint16_t mask_threshold = 3*expset.en_hist_limit;

      if(expset.scan_mode==SCAN_MODE_COUNTING) { // counting
	if(frame_suc)
	  mask_above(buf_frame, buf_frame, data.height*data.width, mask_threshold);
      } else // not counting
	mask_above(data.frame, data.frame, data.height*data.width, mask_threshold);
    }
    
    // --- filtering ---
    if(expset.filtering) {

      uint16_t mask_threshold;
      
      if((expset.filtering==2) && (expinfo.mode==PXC_TPX_MODE_TOT))
	mask_threshold = 11809;
      else if((expset.filtering==3) && (expset.en_hist_limit>0))
	mask_threshold = 3*expset.en_hist_limit;
      else if((expset.filtering==4) && (expset.en_hist_limit>0))
	mask_threshold = 10*expset.en_hist_limit;
      else
	mask_threshold = std::numeric_limits<FRAME_DATA_TYPE>::max();
      
      if(expset.scan_mode==SCAN_MODE_COUNTING) {
	if(frame_suc) {
	  // filter current frame first
	  if(expset.filtering>4)
	    filter_cluster_events1(buf_filtered, buf_frame,
				   (expset.filtering-4), use_calib, expset.detector.mode,
				   data.height, data.width, cluster_wsp);
	  else
	    filter_single_events(buf_filtered, buf_frame, data.height, data.width);
	  // additional pixel masking
	  if((expset.filtering>1) && (mask_threshold<std::numeric_limits<FRAME_DATA_TYPE>::max()))
	    mask_above(buf_filtered, buf_filtered, data.height*data.width, mask_threshold);
	  // sum filtered
	  FRAME_DATA_TYPE * pval = buf_filtered;
	  FRAME_DATA_TYPE * psum = data.filtered;
	  for(size_t i=0; i<DET_FRAME_SZ; i++) {
	    *psum += *pval;
	    psum++; pval++;
	  }
	}
      } else { // not counting
	if(expset.filtering>4)
	  filter_cluster_events1(data.filtered, data.frame,
				 (expset.filtering-4), use_calib, expset.detector.mode,
				 data.height, data.width, cluster_wsp);
	else
	  filter_single_events(data.filtered, data.frame, data.height, data.width);
	// additional pixel masking
	if((expset.filtering>1) && (expset.filtering<=4) && (mask_threshold<std::numeric_limits<FRAME_DATA_TYPE>::max()))
            mask_above(data.filtered, data.filtered, data.height*data.width, mask_threshold);
      }
    } else { // not filtering
      memcpy(data.filtered, data.frame, data.frame_sz*sizeof(FRAME_DATA_TYPE));
    }

    // --- basic statistics ---
    
    // calculated always as it is sometimes required later
    FRAME_DATA_TYPE minval, maxval;
    // frame
    calc_roi_minmax(data.frame, data.height, data.width, expset.detector.rois, DET_NROIS,
		    minval, maxval, use_calib);
    
    expinfo.minValueOriginal = minval;
    expinfo.maxValueOriginal = maxval;

    // filtered
    if(expset.filtering)
      calc_roi_minmax(data.filtered, data.height, data.width, expset.detector.rois, DET_NROIS,
                      minval, maxval, use_calib);
    
    expinfo.minValueFiltered = minval;
    expinfo.maxValueFiltered = maxval;

    // if this is AUTO, it is reseted every time
    if(expset.en_hist_limit==0) {
      expinfo.en_hist_limit = (maxval>DET_ENERGY_NBINS) ? maxval : DET_ENERGY_NBINS; 
    } else {
      expinfo.en_hist_limit = (expset.en_hist_limit>DET_ENERGY_NBINS) ? expset.en_hist_limit : DET_ENERGY_NBINS;
    }

    write_log(LOG_INFO_L2, "Energy histogram limit: %d", expinfo.en_hist_limit);
    
    // --- data statistics ---
    uint16_t frame_events[DET_NROIS], frame_events_filtered[DET_NROIS];
    uint32_t roi_sum[DET_NROIS], roi_sum_filtered[DET_NROIS];
    
    clear_frame_data_postprocess(data, expset.detector.rois); // binning data and histograms

    for(size_t iroi=0; iroi<DET_NROIS; iroi++) {
      frame_events[iroi] = 0; frame_events_filtered[iroi] = 0;
      roi_sum[iroi] = 0; roi_sum_filtered[iroi] = 0;
    }

    calc_roi_stats(data, expset.detector.rois, DET_NROIS,
		   expinfo.en_hist_limit,
		   frame_events, frame_events_filtered,
		   roi_sum, roi_sum_filtered,
		   expset.outputform,
		   use_calib);

    for(size_t iroi=0; iroi<DET_NROIS; iroi++) {
      expinfo.frame_events[iroi]          = frame_events[iroi];
      expinfo.frame_events_filtered[iroi] = frame_events_filtered[iroi];
      expinfo.roi_sum[iroi]               = roi_sum[iroi];
      expinfo.roi_sum_filtered[iroi]      = roi_sum_filtered[iroi];
    }

    write_log(LOG_INFO_L2, "stats calculated");

    // counting mode stop condition
    stop_condition |= (expset.scan_mode==SCAN_MODE_COUNTING) && (expinfo.frame_events_filtered[expset.count_roi]>=expset.count_threshold);

    if((expset.scan_mode==SCAN_MODE_COUNTING) && stop_condition)
      write_log(LOG_INFO, "Counting mode: iframe=%d, events=%u, threshold=%u", iframe, expinfo.frame_events_filtered[expset.count_roi], expset.count_threshold);

    if(frame_suc)
      expinfo.nFrames++; // update number of frames
    
    if((expset.scan_mode!=SCAN_MODE_COUNTING) || stop_condition) {
      // --- data binning ---
      calc_binning_no_gap_no_hot(data, expset.outputform);

      write_log(LOG_INFO_L2, "binning done");
    
      // --- save (DEBUG) ---
      #ifndef PRODUCTION
      savefile_frame_data(data, "/tmp/rite-data/", expset.outputform, iframe);
      savefile_histograms(data, "/tmp/rite-data/", expinfo, expset.outputform, iframe);
      
      write_log(LOG_INFO, "rite-data saved");
      #endif
      
      // --- frame ID ---
      if((expset.scan_mode==SCAN_MODE_COUNTING) || (expset.detector.nFrames==1)) {
	// iframe info not needed
	expinfo.frame_id = (expinfo.start_nb << 8) | (expinfo.exp_nb & 0xff);
      } else {
	// include iframe in frame_id
	expinfo.frame_id = (expinfo.start_nb << 8) | ((expinfo.exp_nb & 0xf) << 4) | (iframe & 0xf);
      }
      
      // --- upload data to DK ---
      start_io_session();
      if(expset.outputform & XRAY_OUTPUTFORM_ENHIST)
	send_energy_histograms(data, expset.detector.rois, DET_NROIS, expinfo.en_hist_limit, expinfo.frame_id);
      if(expset.outputform & (XRAY_OUTPUTFORM_BIN_8 | XRAY_OUTPUTFORM_BIN_16 | XRAY_OUTPUTFORM_BIN_32))
	send_binned_data(data, expset.outputform, expinfo.frame_id);
      if(expset.outputform & XRAY_OUTPUTFORM_HIST)
	send_histograms(data, expset.detector.rois, DET_NROIS, expinfo.frame_id);
      if(expset.outputform & XRAY_OUTPUTFORM_BIN_1)
	send_frame_events(data, expinfo.frame_id, true, expset.detector.rois[(expset.count_roi<DET_NROIS) ? expset.count_roi : 0], expset.outputform & XRAY_OUTPUTFORM_SINGLE_ROI);
      if(expset.outputform & XRAY_OUTPUTFORM_FRAME)
        send_frame_events(data, expinfo.frame_id, false, expset.detector.rois[(expset.count_roi<DET_NROIS) ? expset.count_roi : 0], expset.outputform & XRAY_OUTPUTFORM_SINGLE_ROI);
      end_io_session();
      send_metainfo(expinfo);
    }

    write_log(LOG_INFO_L2, "Detector finished frame nb=%d", iframe);

    if(stop_condition)
      break;
    
  } // iframe

  // release data
  delete[] buf_frame;
  delete[] buf_filtered;
  delete_frame_data(data);
  if(expset.filtering>4)
    delete_filtering_workspace_cluster1(cluster_wsp);

  write_log(LOG_INFO_L2, "frame data deleted");
  write_log(LOG_INFO, "measurement finished (start_nb=%u, exp_nb=%u)", expset.application.start_nb, expset.exp_nb);
  
  return rc;
}

void send_frame_events(const frame_data_t & data, uint16_t id, bool filtered, const roi_settings_t & roi, bool single_roi)
{
  ioEvent_t * ioEvt = new ioEvent_t; // deleted by handler                                                                                                                                                               
  io_frame_data_t * frame = new io_frame_data_t; // deleted by handler
  ioEvt->eventType = XRAY_IO_SEND_FRAME_EVENTS;
  ioEvt->pvData = frame;
  frame->id = id;
  frame->size = data.frame_sz;
  frame->height = data.height;
  frame->width = data.width;
  frame->data = new FRAME_DATA_TYPE[data.frame_sz]; // deleted by handler
  if(filtered) {
    memcpy(frame->data, data.filtered, data.frame_sz*sizeof(FRAME_DATA_TYPE));
    frame->type = 1;
  } else {
    memcpy(frame->data, data.frame, data.frame_sz*sizeof(FRAME_DATA_TYPE));
    frame->type = 0;
  }
  if(single_roi)
    mask_out_of_roi(frame->data, frame->data, frame->height, frame->width, roi);
    
  sIOEventQueue.PostMsg(ioEvt);
}

void send_energy_histograms(const frame_data_t & data, const roi_settings_t rois[], const size_t nrois, uint16_t en_max, uint16_t id)
{
  for(size_t iroi=0; iroi<nrois; iroi++) {

    // skip empty rois
    if(rois[iroi].is_empty() || data.en_hist[iroi]==NULL)
      continue;
        
    ioEvent_t * ioEvt = new ioEvent_t; // deleted by handler                                                                                                                                                                                
    io_energy_histogram_data_t * hist = new io_energy_histogram_data_t; // deleted by handler
    
    ioEvt->eventType = XRAY_IO_SEND_ENERGY_HISTOGRAM;
    ioEvt->pvData = hist;
    hist->id = id;
    hist->roi_id = iroi;
    hist->en_max = en_max;
    
    hist->data = new uint16_t[DET_ENERGY_NBINS]; // deleted by handler
    memcpy(hist->data, data.en_hist[iroi], DET_ENERGY_NBINS*sizeof(uint16_t));

    sIOEventQueue.PostMsg(ioEvt);
    
  } // roi
}

void send_histograms(const frame_data_t & data, const roi_settings_t rois[], const size_t nrois, uint16_t id)
{
  for(size_t iroi=0; iroi<nrois; iroi++) {

    // skip empyt rois
    if(rois[iroi].is_empty() || data.row_hist[iroi]==NULL)
      continue;

    { // row histogram
      ioEvent_t * ioEvt = new ioEvent_t; // deleted by handler
      io_histogram_data_t * hist = new io_histogram_data_t; // deleted by handler

      ioEvt->eventType = XRAY_IO_SEND_HISTOGRAMS;
      ioEvt->pvData = hist;
      hist->id = id;
      hist->roi_id = iroi;

      hist->type = 0; // row 
      hist->len = rois[iroi].height;
            
      hist->data = new uint8_t[hist->len]; // deleted by handler
      memcpy(hist->data, data.row_hist[iroi], hist->len);
      
      sIOEventQueue.PostMsg(ioEvt);
    } // row

    { // column histogram                                                                                                                                                                                                                    
      ioEvent_t * ioEvt = new ioEvent_t; // deleted by handler
      io_histogram_data_t * hist = new io_histogram_data_t; // deleted by handler
      
      ioEvt->eventType = XRAY_IO_SEND_HISTOGRAMS;
      ioEvt->pvData = hist;
      hist->id = id;
      hist->roi_id = iroi;

      hist->type = 1; // column                                                                                                                                                                                                              
      hist->len = rois[iroi].width;

      hist->data = new uint8_t[hist->len]; // deleted by handler
      memcpy(hist->data, data.col_hist[iroi], hist->len);

      sIOEventQueue.PostMsg(ioEvt);
    } // col
  } // iroi
}

void send_binned_data_aux(const FRAME_DATA_TYPE * data, uint32_t len, uint16_t type, uint16_t id)
{
  ioEvent_t * ioEvt = new ioEvent_t; // deleted by handler                                                                                                                                                                                
  io_binned_data_t * bdta = new io_binned_data_t; // deleted by handler                                                                                                                                                             

  ioEvt->eventType = XRAY_IO_SEND_BINNED_DATA;
  ioEvt->pvData = bdta;
  bdta->id = id;
  bdta->type = type;                                                                                                                                                                                                              
  bdta->len = len;

  bdta->data = new FRAME_DATA_TYPE[bdta->len]; // deleted by handler
  memcpy(bdta->data, data, sizeof(FRAME_DATA_TYPE)*bdta->len);

  sIOEventQueue.PostMsg(ioEvt);
}

void send_binned_data(const frame_data_t & data, uint16_t outputform, uint16_t id)
{
  if(outputform & XRAY_OUTPUTFORM_BIN_32)
    send_binned_data_aux(data.bin32, data.frame_sz/(32*32), XRAY_OUTPUTFORM_BIN_32, id);
  if(outputform & XRAY_OUTPUTFORM_BIN_16)
    send_binned_data_aux(data.bin16, data.frame_sz/(16*16), XRAY_OUTPUTFORM_BIN_16, id);
  if(outputform & XRAY_OUTPUTFORM_BIN_8 )
    send_binned_data_aux(data.bin8 , data.frame_sz/( 8*8 ), XRAY_OUTPUTFORM_BIN_8 , id);
}

void start_io_session()
{
  ioEvent_t * ioEvt = new ioEvent_t; // deleted by handler
  ioEvt->eventType = XRAY_IO_START_SESSION;
  ioEvt->pvData = NULL;
  sIOEventQueue.PostMsg(ioEvt);
}

void end_io_session()
{
  ioEvent_t * ioEvt = new ioEvent_t; // deleted by handler
  ioEvt->eventType = XRAY_IO_END_SESSION;
  ioEvt->pvData = NULL;
  sIOEventQueue.PostMsg(ioEvt);
}

void send_metainfo(const experiment_metainfo_t & info)
{
  ioEvent_t * ioEvt = new ioEvent_t; // deleted by handler
  experiment_metainfo_t * ninfo = new experiment_metainfo_t; // deleted by handler

  ioEvt->eventType = XRAY_IO_SEND_METAINFO;
  ioEvt->pvData = ninfo;

  memcpy(ninfo, &info, sizeof(experiment_metainfo_t));
  
  sIOEventQueue.PostMsg(ioEvt);
}

int xray_detector_update(const experiment_settings_t & expset)
{
  // let system to setup for a moment
  usleep(300*1000); // 300 ms

  // detector init (sync, refresh, load config)
  xray_detector_init();

  // bias
  if(expset.detector.bias>1.e-3) {
    double bias;
    pxcGetBias(DET_DEV_IDX, &bias);
    if(abs(expset.detector.bias-bias)>1e-3)
      xray_detector_set_bias(expset.detector.bias);
  }

  // mode
  if(expset.detector.mode != pxcGetTimepixMode(DET_DEV_IDX))
    xray_detector_set_acq_mode(expset.detector.mode);
  
  // threshold
  if(expset.detector.threshold>1.e-3) {
    double threshold;
    pxcGetThreshold(DET_DEV_IDX, 0, &threshold);
    if(abs(expset.detector.threshold-threshold)>1e-3)
      xray_detector_set_threshold(expset.detector.threshold);
  }

  return 0;
}

int new_global_detector_calibration()
{
  g_det_calib_avail = false;
  
  try {
    g_det_caliba = new float[DET_FRAME_SZ]();
    g_det_calibb = new float[DET_FRAME_SZ]();
    g_det_calibc = new float[DET_FRAME_SZ]();
    g_det_calibt = new float[DET_FRAME_SZ]();
  } catch (std::exception & e) {
    write_log(LOG_ERROR, "Error when allocating memory for detector calibration matrices");
    return 1;
  }

  float *p = g_det_caliba;
  for(size_t ipix=0; ipix<DET_FRAME_SZ; ipix++) {
    *p = 1.;
    p++;
  }
    
  return 0;
}

int delete_global_detector_calibration()
{
  g_det_calib_avail = false;
  
  try {
    if(g_det_caliba) {
      delete [] g_det_caliba;
      g_det_caliba = NULL;
    }
    if(g_det_calibb) {
      delete [] g_det_calibb;
      g_det_calibb = NULL;
    }
    if(g_det_calibc) {
      delete [] g_det_calibc;
      g_det_calibc = NULL;
    }
    if(g_det_calibt) {
      delete [] g_det_calibt;
      g_det_calibt = NULL;
    }
  } catch (std::exception & e) {
    write_log(LOG_ERROR, "Error when releasing memory for detector calibration matrices");
    return 1;
  }

  return 0;
}

int load_calib_file_aux(float * calib, const char * fname)
{
  int rv;
  int rv_rd = 0;
  FILE * fid = fopen(fname, "rb");

  if(fid==0) {
    write_log(LOG_ERROR, "Error opening file: %s", fname);
    return 1;
  }

  rv = fread((void*)calib, sizeof(float), DET_FRAME_SZ, fid);
  if(rv!=DET_FRAME_SZ) {
    write_log(LOG_ERROR, "Error reading file: %s", fname);
    rv_rd = 1;
  }
  
  rv = fclose(fid);
  fid = 0;
  if(rv!=0) {
    write_log(LOG_ERROR, "Error closing file: %s", fname);
    return 1;
  }

  return rv_rd;
}
			
int load_detector_calibration(const char * detector_name)
{
  std::string str;
  int err = 0;

  g_det_calib_avail = false;
  
  str = "/opt/rite/configs/" + std::string(detector_name) + "_caliba.txt";
  if(!err)
    err = load_calib_file_aux(g_det_caliba, str.c_str());

  str = "/opt/rite/configs/" + std::string(detector_name) + "_calibb.txt";
  if(!err)
    err = load_calib_file_aux(g_det_calibb, str.c_str());

  str = "/opt/rite/configs/" + std::string(detector_name) + "_calibc.txt";
  if(!err)
    err = load_calib_file_aux(g_det_calibc, str.c_str());

  str = "/opt/rite/configs/" + std::string(detector_name) + "_calibt.txt";
  if(!err)
    err = load_calib_file_aux(g_det_calibt, str.c_str());
  
  if(err) {
    float * pa = g_det_caliba;
    for(size_t ipix=0; ipix<DET_FRAME_SZ; ipix++) {
      *pa = 1.;
      pa++;
    }
    float * pb = g_det_calibb;
    for(size_t ipix=0; ipix<DET_FRAME_SZ; ipix++) {
      *pb = 0.;
      pb++;
    }
    float * pc = g_det_calibc;
    for(size_t ipix=0; ipix<DET_FRAME_SZ; ipix++) {
      *pc = 0.;
      pc++;
    }
    float * pt = g_det_calibt;
    for(size_t ipix=0; ipix<DET_FRAME_SZ; ipix++) {
      *pt = 0.;
      pt++;
    }
    g_det_calib_avail = false;
  }

  g_det_calib_avail = (err==0);
  
  return err;
}
