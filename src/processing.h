/*
 * processing.h
 *
 *  Author: Zdenek Matej
 */ 

#include <limits>
#include <fstream>
#include "Log.h"
#include "xray.h"
#include "detectorTask.h"

#ifndef _XRAY_PROCESSING_H_
#define _XRAY_PROCESSING_H_

#define FRAME_DATA_TYPE  unsigned short
#define FRAME_DIM_TYPE unsigned char

float tot_to_energy(size_t ipix, FRAME_DATA_TYPE tot, bool use_calib);
FRAME_DATA_TYPE energy_to_tot(size_t ipix, float en, bool use_calib);

template <class T> int save_file_bin(const T * data, size_t sz, const char * filename)
{
  using namespace std;
  
  ofstream fs(filename, ios::out | ios::binary);
  if(!fs) {
    write_log(LOG_ERROR, "Error when opening file %s", filename);
    return 1;
  }

  fs.write((const char*) data, sz*sizeof(T));

  fs.close();
  if(!fs.good()) {
    write_log(LOG_ERROR, "Error when writing to file %s", filename);
    return 1;
  }
  return 0;
}

template <class T> void calc_data_stats(const T * data, size_t sz, unsigned int & numEvents, T & minval, T & maxval)
{
  numEvents = 0;
  minval = std::numeric_limits<T>::max();
  maxval = std::numeric_limits<T>::min();

  const T * p = data;
  
  for(size_t i=0; i<sz; i++) {
    const T val = *p;
    if(val != 0)
      numEvents++;
    if(val>maxval)
      maxval = val;
    if(val<minval)
      minval = val;
    p++;
  }
}

template <class T> void calc_roi_minmax(const T * frame, size_t height, size_t width,
					const roi_settings_t rois[], size_t nrois,
					T & minval, T & maxval, bool use_calib)
{
  minval = std::numeric_limits<T>::max();
  maxval = std::numeric_limits<T>::min();

  bool active[DET_NROIS];

  for(size_t iroi=0; iroi<nrois; iroi++)
    active[iroi] = ! rois[iroi].is_empty();
  
  const T * p = frame;
  size_t ipix = 0;
  
  for(size_t irow=0; irow<height; irow++) {
    for(size_t icol=0; icol<width; icol++) {
      T val = (use_calib) ? (T)(int)(tot_to_energy(ipix, *p, use_calib)+0.5f) : (*p);
      for(size_t iroi=0; iroi<nrois; iroi++) {
        if(active[iroi] && (icol>=rois[iroi].col) && (irow>=rois[iroi].row) && (icol<(rois[iroi].col+rois[iroi].width)) && (irow<(rois[iroi].row+rois[iroi].height))) {
          if(val>maxval)
            maxval = val;
          if((val!=0) && (val<minval))
            minval = val;
        }
      }	// iroi
      p++;
      ipix++;
    } // icol
  } // irow
}

struct frame_data_t
{
  FRAME_DATA_TYPE * frame;
  FRAME_DATA_TYPE * filtered;
  FRAME_DATA_TYPE * bin8;
  FRAME_DATA_TYPE * bin16;
  FRAME_DATA_TYPE * bin32;
  int frame_sz;
  int height;
  int width;
  uint8_t  * col_hist[DET_NROIS];
  uint8_t  * row_hist[DET_NROIS];
  uint16_t * en_hist[DET_NROIS];
};

template <class T> void calc_roi_stats(frame_data_t data,
				       const roi_settings_t rois[], const size_t nrois,
				       T en_hist_limit,
				       uint16_t frame_events[], uint16_t frame_events_filtered[],
				       uint32_t roi_sum[], uint32_t roi_sum_filtered[],
				       uint16_t outputform,
				       bool use_calib)
{
  float ebinwidth = en_hist_limit/float(DET_ENERGY_NBINS);

  bool calc_hist   = (outputform & XRAY_OUTPUTFORM_HIST) > 0;
  bool calc_enhist = (outputform & XRAY_OUTPUTFORM_ENHIST) > 0;
  
  bool active[DET_NROIS];
  uint16_t rowEvents[DET_NROIS];
  uint32_t zeroEvents[DET_NROIS] = { 0 };
  
  for(size_t iroi=0; iroi<nrois; iroi++) {
    active[iroi] = ! rois[iroi].is_empty();
    zeroEvents[iroi] = 0;
    //printf("%d [%d, %d, %d, %d]\n", iroi, rois[iroi].row, rois[iroi].col, rois[iroi].height, rois[iroi].width);
  }

  const T * pd = data.frame;
  const	T * pf = data.filtered;
  size_t ipix = 0;
  
  for(size_t irow=0; irow<data.height; irow++) {
    for(size_t iroi=0; iroi<nrois; iroi++)
      rowEvents[iroi] = 0;
    for(size_t icol=0; icol<data.width; icol++) {
      T	dval = *pd;
      T fval = *pf;
      for(size_t iroi=0; iroi<nrois; iroi++) {
        if(active[iroi] && (icol>=rois[iroi].col) && (irow>=rois[iroi].row) && (icol<(rois[iroi].col+rois[iroi].width)) && (irow<(rois[iroi].row+rois[iroi].height))) {
	  if(dval > 0) {
	    roi_sum[iroi] += dval;
            frame_events[iroi]++;
	  }
	  if(fval > 0) {
	    roi_sum_filtered[iroi] += fval;
	    frame_events_filtered[iroi]++;
	    rowEvents[iroi]++;
	    // histogram
	    if(calc_hist) {
	      size_t dcol = icol - rois[iroi].col;
	      if(data.col_hist[iroi][dcol]<255)
		data.col_hist[iroi][dcol]++;
	    }
	    // energy histogram
	    if(calc_enhist) {
	      size_t ien = size_t((use_calib ? tot_to_energy(ipix, fval, use_calib) : (float)fval)/ebinwidth); // fval>0
	      if(ien==0)
		zeroEvents[iroi]++;
	      else if(ien<DET_ENERGY_NBINS) {
		uint16_t val = data.en_hist[iroi][ien];
		if(val<std::numeric_limits<uint16_t>::max())
		  data.en_hist[iroi][ien] = val+1;
	      } else if(fval==en_hist_limit) {
		uint16_t val = data.en_hist[iroi][DET_ENERGY_NBINS-1];
                if(val<std::numeric_limits<uint16_t>::max())
                  data.en_hist[iroi][DET_ENERGY_NBINS-1] = val+1;
	      }
	    } // calc_enhist
	  } //else { // else fval>0 // Note: true zero events are excluded 
	    //zeroEvents[iroi]++;
	  //}
	} // active and pixel in roi
      } // iroi 
      pd++;
      pf++;
      ipix++;
    } // icol

    if(calc_hist) {
      for(size_t iroi=0; iroi<nrois; iroi++) {
	if(active[iroi] && (irow>=rois[iroi].row) && (irow<(rois[iroi].row+rois[iroi].height))) {
	  size_t drow = irow - rois[iroi].row;
	  uint16_t _s = data.row_hist[iroi][drow] + rowEvents[iroi];
	  data.row_hist[iroi][drow] = (_s<255) ? _s : 255;
	}
      }
    }
  } // irow

  // energy histogram zero bin
  if(calc_enhist) {
    for(size_t iroi=0; iroi<nrois; iroi++) {
      if(active[iroi]) {
	uint16_t val = data.en_hist[iroi][0];
	if((zeroEvents[iroi]<=std::numeric_limits<uint16_t>::max()) && (val<=(std::numeric_limits<uint16_t>::max()-zeroEvents[iroi])))
	  data.en_hist[iroi][0] += zeroEvents[iroi];
	else
	  data.en_hist[iroi][0] = std::numeric_limits<uint16_t>::max();
      }
    }
  }
}

void calc_binning_no_gap_no_hot(frame_data_t & data, uint16_t outputform);
int new_frame_data(frame_data_t & data, const roi_settings_t rois[]);
int delete_frame_data(frame_data_t & data);
int clear_frame_data_postprocess(frame_data_t & data, const roi_settings_t rois[]);
int savefile_frame_data(const frame_data_t & data, const char * dirname, uint16_t outputform, int iframe);
int savefile_histograms(const frame_data_t & data, const char * dirname, const experiment_metainfo_t & info, uint16_t outputform, int iframe);
int filter_single_events(FRAME_DATA_TYPE * filtered, const FRAME_DATA_TYPE * frame, size_t height, size_t width);
int mask_above(FRAME_DATA_TYPE * out, const FRAME_DATA_TYPE * in, size_t len, FRAME_DATA_TYPE threshold);
int mask_out_of_roi(FRAME_DATA_TYPE * out, const FRAME_DATA_TYPE * in, size_t height, size_t width, const roi_settings_t & roi);

#endif /* _XRAY_PROCESSING_H_ */
