#include <thread>
#include <mutex>
#include <condition_variable>

#include <iostream>

#ifndef _CONSUMABLE_VALUE_H_
#define _CONSUMABLE_VALUE_H_
 
template <typename T>
class ConsumableValue
{
  public:
 
  template< class Rep, class Period >
  bool produce(const T& item, const std::chrono::duration<Rep, Period>& rel_time)
  {
    std::cv_status status = std::cv_status::no_timeout;
    std::unique_lock<std::mutex> mlock(mutex_);
    if (valid_)
      status = cond_.wait_for(mlock, rel_time);
    if(status==std::cv_status::no_timeout && !valid_) {
      value_ = item;
      valid_ = true;
			mlock.unlock();
			cond_.notify_one(); 
      return false;
    } else {
      return true; // err
    }
  }
  
  template< class Rep, class Period >
  bool consume(T& item, const std::chrono::duration<Rep, Period>& rel_time)
  {
    std::cv_status status = std::cv_status::no_timeout;
    std::unique_lock<std::mutex> mlock(mutex_);
    if (!valid_)
      status = cond_.wait_for(mlock, rel_time);
    if(status==std::cv_status::no_timeout && valid_) {
      item = value_;
			valid_ = false;
			mlock.unlock();
			cond_.notify_one();
      return false;
    } else {
      return true; // err
    }
  }
 
  ConsumableValue():valid_(false) {};
  
 private:
  T value_;
  bool valid_;
  std::mutex mutex_;
  std::condition_variable cond_;
};

#endif /* _CONSUMABLE_VALUE_H_ */
