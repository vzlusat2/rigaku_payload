# RITE payload for vzlusat2 mission

*Mars ZX2 control firmware for RITE payload for vzlusat2 mission*

- RITE is based on (c)Enclustra [Mars ZX2](https://www.enclustra.com/en/products/system-on-chip-modules/mars-zx2/) (c)Xilinx Zynq 7010 All Programmable SoC with additional componets from (c)[VZLU](https://www.vzlu.cz) and (c)[Eltvor](https://www.eltvor.cz/).
- RITE-vzlusat2 is using customized (c)[Advacam](https://advacam.com/) Timepix camera with CdTe sensor
- Control communication is handled via [Cube Space Protocol](https://github.com/libcsp/libcsp) and data are sent to Data Keeper (DK) module ((c)VZLU).
- RITE-vzlusat2 is a continuation of vzlusat1 mission. API and some parts of this software are heavily utilizing experience from the first mission: (c)[Klaxalk on GitHub](https://github.com/klaxalk/vzlusat-timepix) and [vzluat1-timepix-decoder](https://github.com/klaxalk/vzlusat-timepix-decoder)

Special thanks to
- Martin Sabol, Petr Svoboda (VZLU)
- Ondrej Petr (RITE)
- Marek Peca (Eltvor)
- Daniela Doubravova (Advacam)
- [Tomas Baca](https://github.com/klaxalk) (CVUT)

# References

1. Tomas baca et al., [Miniaturized X-ray telescope for VZLUSAT-1 nanosatellitewith Timepix detector](http://mrs.felk.cvut.cz/data/papers/iworid_2016_tomas_baca.pdf), (2018)
2. Tomas Baca et al., [Timepix in LEO Orbit onboard the VZLUSAT-1Nanosatellite: 1-year of Space Radiation DosimetryMeasurements](http://mrs.felk.cvut.cz/data/papers/jinst_2018_vzlusat.pdf), (2018)
3. Tomas Baca et al., [Timepix  Radiation  Detector  for  Autonomous  Radiation  Localizationand  Mapping  by  Micro  Unmanned  Vehicles](http://mrs.felk.cvut.cz/data/papers/iros_2019_timepix.pdf), (2019)

# Other components

- [xray](https://gitlab.com/vzlusat2/xray) API commands
- [recon](https://gitlab.com/vzlusat2/recon) utility for decoding DK RITE data
- [vcom](https://gitlab.com/vzlusat2/vcom) control terminal (private repo)
- [vcom-dump](https://gitlab.com/vzlusat2/vcom-dump)  utility to dump RITE data from DK (private repo)

# Building

## Crosscompiling for Mars ZX2

- build using [buildroot](https://buildroot.org/) environment (release2020.02)
- required CMake version: 3.17.3
- using Ninja  
- add path of (c)Advacam PIXet SDK library (Mars ZX2 ARM edition, eabihf) to `LD_LIBRARY_PATH` environment variable 
- execute `./nbuild_mars_zx2.sh` script

## Linux

- required CMake version: 3.16.3
- using Linux make
- add path of (c)Advacam PIXet SDK library (172.b857.Linux) to `LD_LIBRARY_PATH` environment variable
- execute `./mbuild_linux.sh` script
