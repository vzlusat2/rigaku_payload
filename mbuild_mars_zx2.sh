#!/bin/bash
mkdir -p build
cd build
cmake .. \
    -DCMAKE_TOOLCHAIN_FILE=../enclustra_toolchain.cmake \
    -DCMAKE_BUILD_TYPE=Debug \
    -Duse_libsocketcan=ON \
    --oenable-shared
make
