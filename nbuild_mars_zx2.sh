#!/bin/bash
mkdir -p build
cd build
cmake -GNinja .. \
    -DCMAKE_TOOLCHAIN_FILE=../enclustra_toolchain.cmake \
    -DCMAKE_BUILD_TYPE=Debug \
    -Duse_libsocketcan=ON -DSOCKETCAN_PREFIX=/home/vagrant \
    -DCMAKE_LIBRARY_PATH=$LIBRARY_PATH
ninja
